<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>participant preview page</title>
  <link rel="stylesheet" href="table.css">
</head>
<body>
  <div class="toast">
  </div>
  <header>
    <h1>Hi, this is signed_up users table</h1>
  </header>
  <div class="signed-table">
    <table id='sgnd'>
      <thead>
        <tr>
          <th>id</th>
          <th>name surname</th>
          <th>birthday</th>
          <th>phone</th>
          <th>email</th>
          <th>parents</th>
          <th>study form</th>
          <th>course</th>
          <th>occupation</th>
          <th>lessontime</th>
          <th>hours per month</th>
          <th>city</th>
          <th>english</th>
          <th>portfolio</th>
          <th class='about-th'>about</th>
          <th>photo</th>
          <th>created_at</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
  <?php
    include '../backend/dbAnketa.php';
    $query = "SELECT * FROM `users` WHERE `role_id` =1 ";
    $participants = mysqli_query($con, $query);

    if ($participants->num_rows > 0) {
      $n = 1;
    	while ($row = mysqli_fetch_array($participants, MYSQL_ASSOC)) {
        ?>
        <tr <?php if( $n%2 == 1 ) echo 'class="alt";'?>>
          <td><?=$row['id']?></td>
          <td><?=$row['name']. ' ' . $row['surname']?></td>
          <td>
            <?php
              echo $row['birthday'];
              $birthDate = explode('-', $row['birthday']);
              $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")  ? ((date("Y") - $birthDate[2]) - 1) : (date("Y") - $birthDate[2]));
              $age = date("Y") - explode('-', $row['birthday'])[0];
              echo ' ('.$age.' years)';
            ?>
          </td>
          <td><?=$row['phone']?></td>
          <td><?=$row['email']?></td>
          <td><?php if($row['parent_phone'] != '' || $row['parent_name'] != '') echo $row['parent_phone'] . ' (' . $row['parent_name']. ')'?></td>
          <td><?=$row['studyform']?></td>
          <td><?=$row['course']?></td>
          <td><?=$row['occupation']?></td>
          <td><?=$row['lessontime']?></td>
          <td><?=$row['hours']?></td>
          <td><?=$row['city']?></td>
          <td><?=$row['english']?></td>
          <td><?=$row['portfolio']?></td>
          <td><?=$row['about']?></td>
          <td>
            <div class="spoiler-wrapper">
              <div class="spoiler folded">show&nbsp;/&nbsp;hide</div>
              <div class="spoiler-content">
                <?= "<img src='../backend/".$row['photo']."' alt='' height=150>"?>
              </div>
            </div>
          </td>
          <td><?=$row['created_at']?></td>
          <td>
            <button class='table-button pre-user-delete-button' type="button" >delete</button>
            <div class="table-aprove-block">
              User data will be deleted forever! You'll never see it again. Are you sure?
              <button class='table-button user-delete-button' type="button" data-id='<?=$row['id']?>'>Delete forever</button>
              <button class='table-button cancel-user-delete-button' type="button" >Canel</button>
            </div>
          </td>
        </tr>
        <?php
        $n++;
      }
    }
  ?>
      </tbody>
    </table>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js" charset="utf-8"></script>
  <script src="signedTable.js" charset="utf-8"></script>
</body>
</html>
