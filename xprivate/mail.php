<!DOCTYPE html>
<html lang="ru-UA">
<head>
    <meta charset="UTF-8">
    <title>Email sender</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <style>
        body {min-width: 1000px;}
        form {padding: 20px;}
        textarea {resize: none;}
        tr, th, td {padding: 10px;}
        th {width: 300px;}
        td {width: 800px;}
        td input {width: 100%;}
        td textarea {width: 100%; height: 200px;}
        td div.add {border: 1px dotted black; padding: 5px; margin: 5px 0; width: 200px;}
        table {margin-bottom: 20px;}
        #result {margin: 20px;padding: 10px;width: 760px;}
        .success {color: DarkGreen;}
        .error {color: red;}
        input[type="checkbox"], input[type="radio"] {width: 17px; height: 17px; vertical-align: top; margin: 1px 5px 0 0;}
        button {margin-top: 10px;}
        .initial_font {font-weight: initial;}
    </style>
</head>
<body>
<?php
function logEmail($logtype, $sendto, $theme, $message)
{
    file_put_contents('maillogs/mail' . date('Y-m') . '.log',
        '------------------------' . PHP_EOL .
        date('d.m.Y H:i') . ' ' . $logtype . PHP_EOL .
        'SENDTO: ' . $sendto . PHP_EOL .
        'THEME: ' . $theme . PHP_EOL .
        'TEXT: ' . $message . PHP_EOL, FILE_APPEND);
}

function sendEmail($sendTo, $theme, $message, $sendType = 'admin')
{
    if (!is_array($sendTo)) {
        $sendTo = [$sendTo];
    }

    // Create the mail transport configuration
    $transportForUser = Swift_MailTransport::newInstance();

    // Create the message
    $messageForUser = Swift_Message::newInstance()->setCharset('UTF-8');

    if ($sendType === 'admin') {
        $messageForUser->setTo(['info@programming.kr.ua']);
        // $messageForUser->setBcc(array(
        //     'oliinyk.artem@gmail.com'
        // ));
    } else if ($sendType === 'addToReceivers') {
        $messageForUser->setTo($sendTo);
        $messageForUser->setCc(['info@programming.kr.ua']);
    } else if ($sendType === 'addToReceiversWithoutCopy') {
        $messageForUser->setTo($sendTo);
        // $messageForUser->setCc(array(
        //     'info@programming.kr.ua'
        // ));
    } else if ($sendType === 'addToBcc') {
        $messageForUser->setTo(['info@programming.kr.ua']);
        $messageForUser->setBcc($sendTo);
    } else {
        return false;
    }

    $messageForUser->setContentType('text/html');
    $messageForUser->setSubject($theme);
    $messageForUser->setBody(nl2br($message));

    $mailFrom = !empty($_POST['fromMail']) ? $_POST['fromMail'] : 'info';
    $mailName = !empty($_POST['fromName']) ? $_POST['fromName'] : 'Ш++';

    $messageForUser->setFrom($mailFrom . '@programming.kr.ua', $mailName);

    // Send the email
    $mailer = Swift_Mailer::newInstance($transportForUser);
    $mailer->send($messageForUser);

    // Add to logs
    $logRecipient = '';
    foreach ($sendTo as $rec) {
        $logRecipient .= $rec . ' | ';
    }

    logEmail('SUCCESS', $logRecipient, $theme, $message);
}

if (isset($_POST['recipient']) && isset($_POST['theme']) && isset($_POST['message'])) {
    $recipients = explode(PHP_EOL, $_POST['recipient']);
    $theme = $_POST['theme'];
    $message = $_POST['message'];

    echo "<div id='result' class='table-bordered'>";

    require_once '../backend/lib/swift_required.php';

    if (isset($_POST['hideReceivers'])) {
        $allValidRec = [];

        foreach ($recipients as $recipient) {
            $recipient = trim($recipient);
            if (filter_var($recipient, FILTER_VALIDATE_EMAIL)) {
                if (!in_array($recipient, $allValidRec)) {
                    $allValidRec[] = $recipient;
                    echo "<span class='success'><b>Отправлено сообщение</b>: $recipient</span><br>";
                }
            } else {
                if ($recipient != '') {
                    logEmail('ERROR', $recipient, $theme, $message);
                    echo "<span class='error'><b>Сообщение не отправлено</b>: $recipient</span><br>";
                }
            }
        }

        if (count($allValidRec) == 0) {
            echo "<span class='error'><b>Сообщение не было отправлено ни одному получателю!!!</b></span><br>";
        } else {
            if ($_POST['hideReceivers'] === 'off') {
                sendEmail($allValidRec, $theme, $message, 'addToReceivers');
            } else if ($_POST['hideReceivers'] === 'on') {
                sendEmail($allValidRec, $theme, $message, 'addToBcc');

                $adminMessage = 'Получатели были добавлены в скрытую копию<br><b>ПОЛУЧАТЕЛИ</b>: ';
                foreach ($allValidRec as $rec) {
                    $adminMessage .= $rec . ' | ';
                }
                $adminMessage .= '<br><b>СООБЩЕНИЕ</b>: <br>' . $message;
                sendEmail('info@programming.kr.ua', $theme, $adminMessage, 'admin');
            } else if ($_POST['hideReceivers'] === 'diffmsg') {
                $adminMessage = 'Получателям отправлены разные сообщения<br><b>ПОЛУЧАТЕЛИ</b>: ';

                foreach ($allValidRec as $rec) {
                    $adminMessage .= $rec . ' | ';
                    $newmsg = preg_replace("/\{\{email\}\}/", $rec, $message);
                    sendEmail($rec, $theme, $newmsg, 'addToReceiversWithoutCopy');
                }

                $adminMessage .= '<br><b>СООБЩЕНИЕ</b>: <br>' . $message;
                sendEmail('info@programming.kr.ua', $theme, $adminMessage, 'admin');
            }
        }
    } else {
        echo "<span class='error'><b>Not enough post data! Messages weren't send!</b></span><br>";
    }
    $message = nl2br($message);
    echo "<u>Тема</u>: $theme<br>";
    echo "<u>Сообщение</u>:<br> $message";
    echo '</div>';
}
?>
<form action="" method="POST">
    <table>
        <tr>
            <th>
                <label for="fromMail">Email, от которого прийдет письмо <br>(автоматически будет добавлено <i>@programming.kr.ua</i>)</label>
            </th>
            <td><input id="fromMail" name="fromMail" type="text" value="info" required/></td>
        </tr>
        <tr>
            <th><label for="fromName">Имя, от которого прийдет письмо</label></th>
            <td><input id="fromName" name="fromName" value="Ш++" required/></td>
        </tr>
        <tr>
            <th><label for="recipient">Email-ы получателей<br>(каждый с новой строки)</label></th>
            <td><textarea id="recipient" name="recipient" required></textarea></td>
        </tr>
        <tr>
            <th><label for="theme">Тема сообщения</label></th>
            <td><input id="theme" name="theme" required/></td>
        </tr>
        <tr>
            <th><label for="message">Текст сообщения<br>ентеры соответствуют ентерам в тексте</label></th>
            <td><textarea id="message" name="message" required>


Ваша Ш++</textarea>
            </td>
        </tr>
        <tr>
            <th>Заметки</th>
            <td>
                1. Получателей нужно вводить каждого с новой строки. <br>
                2. Сообщения будут отправлены каждому получателю отдельно (один получатель не сможет увидеть email
                другого). <br>
                3. Сообщение будет отправлено с адреса <b>info@programming.kr.ua</b> (имя отправителя &mdash; <b>Ш++</b>). <br>
                4. Сообщение должно быть написано в формате <b>html</b>: <br>
                &nbsp; - ентеры конвертируются автоматически для дополнительного ентера &lt;br&gt; <br>
                &nbsp; - ссылка &lt;a href="http://somelink"&gt;Text of link&lt;/a&gt; <br>
                &nbsp; - выделить жирным &lt;b&gt;Sometext&lt;/b&gt; <br>
                &nbsp; - выделить курсивом &lt;i&gt;Sometext&lt;/i&gt; <br>
                &nbsp; - подчеркнуть &lt;u&gt;Sometext&lt;/u&gt;<br>
                5. В третьем режиме отправки сообщений можно использовать <b>{{email}}</b>, вместо которого подставиться
                email получателя <br>
            </td>
        </tr>
    </table>
    <input type="radio" id="hide-recievers-off" name="hideReceivers" value="off" required/>
    <label for="hide-recievers-off" class="initial_font">
        - Получатели БУДУТ видеть друг друга (отправиться 1 письмо, все имейлы будут добавлены в получатели)
    </label>
    <br>
    <input type="radio" id="hide-recievers-on" name="hideReceivers" value="on" required/>
    <label for="hide-recievers-on" class="initial_font">
        - Получатели НЕ БУДУТ видеть друг друга (отправиться 1 письмо, получатели будут добавлены в скрытую копию)
    </label>
    <br>
    <input type="radio" id="hide-receivers-diff-msg" name="hideReceivers" value="diffmsg" required/>
    <label for="hide-receivers-diff-msg" class="initial_font"> - Получатели НЕ БУДУТ видеть друг друга (отправятся РАЗНЫЕ
        письма, можно использовать <b>{{email}}</b> )</label>
    <br>
    <button class="btn btn-success">Отправить сообщения</button>
</form>
</body>
</html>
