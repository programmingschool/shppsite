$(document).ready(function() {
  $('.spoiler-text').hide()
  $('.spoiler').click(function() {
    $(this).toggleClass("folded").toggleClass("unfolded").next().slideToggle()
  })

  $('.pre-user-delete-button').click(function(event) {
    var parentTd = $(this).parent();
    $(this).slideUp('fast')
    parentTd.find('.table-aprove-block').slideDown('fast');
  });

  $('.cancel-user-delete-button').click(function(event) {
    var parentTd = $(this).parent().parent();
    parentTd.find('.table-aprove-block').slideUp('fast');
    parentTd.find('.pre-user-delete-button').slideDown('fast');
  });

  $('.user-delete-button').click(function(event) {
    userId = $(this).data('id');
    deletButton = $(this);

    $.ajax({
        url: 'deleteUser.php',
        type: 'POST',
        data: {
          userId: userId
        }
      })
      .done(function(response) {
        if (response) {
          deletButton.closest('tr').remove();
          showToast('User were deleted', 2000);

        } else {
          showToast('Something went wrong. WTF!??, please call your admin', 5000);
        }
      })
      .fail(function() {
        showToast('Something went wrong. WTF!??, please call your admin', 5000);
        console.log("error");
      });
  });

  function showToast(text, delay) {
    toast = $('.toast');
    toast.text(text);
    toast.slideDown();
    setTimeout(function() {
      toast.slideUp()
    }, delay);
  }

  $('#sgnd').DataTable({
    "columnDefs": [{
      "width": "20%",
      "targets": 13
    }],
    "lengthMenu": [
      [25, 50, -1],
      [25, 50, "All"]
    ],
    "aoColumnDefs": [{
      "bSortable": false,
      "aTargets": [17, 15, 5]
    }]
  });
});
