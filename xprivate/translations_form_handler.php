<?php
if (empty($_POST)) {
    header('location: ./translations.php');
}
session_start();
if (!isset($_POST['lang'])) {
    $_SESSION['error'] = 'Не указан идентификатор языка';
    header('location: ./translations.php');
    return;
}
$lang = $_POST['lang'];
unset($_POST['lang']);
$langFile = json_encode($_POST, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
$translationPath = dirname(__DIR__) . '/translations';
$translationFilePath = $translationPath . '/' . $lang . '.json';

if (!copy($translationFilePath, $translationPath . '/' . $lang . '_backup_' . time() . '.json')) {
    $_SESSION['error'] = '🚫 Не удалось сделать бэкап старого перевода. Изменения не сохранены';
    header('location: ./translations.php?tab=' . $lang);
    return;
}

if (false === @file_put_contents($translationFilePath, $langFile)) {
    $_SESSION['error'] = '🚫 Не удалось сохранить перевод. Изменения не сохранены 😭';
    header('location: ./translations.php?tab=' . $lang);
    return;
}

$_SESSION['message'] = '✔️ Изменения сохранены. Очистите кэш браузера, для проверки изменений на сайте. Хорошего дня 😊';
header('location: ./translations.php?tab=' . $lang);
