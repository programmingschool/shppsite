<?php
  header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
  header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
  header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");

  clearstatcache();

  $pardir = basename(dirname(__FILE__));
  // $dir = ($pardir == "_edit") ? "../" : "";
  $dir = "../";
  $basename = basename(__FILE__, '.php');
  
  if (isset($_POST["content"]))
    file_put_contents($dir.$basename, ($_POST["content"]));
  
  if (!file_exists($dir.$basename)) {
    file_put_contents($dir.$basename, "");
    chmod($dir.$basename, 0666);
  }

  $src = (file_exists('logo.png')) ? 'logo.png' : '';
  $src = (file_exists($basename.'.logo.png')) ? $basename.'.logo.png' : $src;
  $src = (file_exists($basename.'.logo.lnk')) ? file_get_contents($basename.'.logo.lnk') : $src;
  $src = (file_exists('logo.lnk')) ? file_get_contents('logo.lnk') : $src;

  $descr = $basename;
  $descr = (file_exists($basename.'.descr')) ? file_get_contents($basename.'.descr') : $descr;
  $descr = (file_exists('common.descr')) ? file_get_contents('common.descr') : $descr;

  $file = file_get_contents($dir.$basename);
?><!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="cache-control" content="max-age=0" />
  <meta http-equiv="cache-control" content="no-cache" />
  <meta http-equiv="expires" content="-1" />
  <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
  <meta http-equiv="pragma" content="no-cache" />
  <title>Simple Editor</title>
  <style>
    body,html{margin:0;height:100%}*{font-family:"Courier New",Courier,monospace}.form{height:100%;width:100%;min-width:600px}h3{white-space:nowrap}.content{height:86%;margin-top:20px;margin-left:20px;margin-bottom:20px}.subm{float:right}.nav{background-color:#101010;padding-left:20px;padding-right:20px;height:60px;min-width:600px}.header{color:#999;float:left;max-width:800px;overflow:hidden}#mySubmit{border:none;background-color:#101010;border-left:2px solid #353535;border-right:2px solid #353535;margin:0;height:60px;text-transform:uppercase;font-size:30px;color:#fff;cursor:pointer;width:100px}#mySubmit:disabled{cursor:auto;background-color:#101010;color:#464444;border-left:2px solid #000;border-right:2px solid #353535}textarea{width:100%;height:100%;resize:none;padding:0;overflow-y:scroll;border:none;outline:0;font-size:18px}::-webkit-scrollbar{width:15px}.logo{float:left;margin-right:20px;}.logo img{height:60px}
  </style>
</head>
<body>
  <div class="form">
    <div class="nav">
      <div class="logo">
        <?php if ($src !== '') echo '<img src="'.$src.'">'; ?>
      </div>
      <div class="header">
        <h3><?php echo $descr; ?></h3>
      </div>
      <div class="subm">
        <button onclick="submForm()" id="mySubmit">Save</button>
      </div>
    </div>
    <div class="content">
      <textarea name="content" id="fileContent" autofocus><?php echo $file; ?></textarea>
    </div>
  </div>
  <script>
    var textarea = document.getElementById("fileContent");
    var submit = document.getElementById("mySubmit");
  
    submit.disabled = true;
    
    textarea.onkeydown = function(e) {
      if (e.keyCode == 9 || e.which == 9) {
        e.preventDefault();
        
        var s = this.selectionStart;
        
        this.value = this.value.substring(0, this.selectionStart) + "\t" + this.value.substring(this.selectionEnd);
        this.selectionEnd = s + 1; 
        submit.disabled = false;
      }
      if (e.keyCode == 13 || e.which == 13) {
        e.preventDefault();
        
        var s = this.selectionStart;
        
        this.value = this.value.substring(0, this.selectionStart) + "\n" + this.value.substring(this.selectionEnd);
        this.selectionEnd = s + 1; 
        submit.disabled = false;
      }
      if (((e.keyCode == 86 && e.ctrlKey) || (e.which == 86 && e.ctrlKey)) || ((e.keyCode == 90 && e.ctrlKey) || (e.which == 90 && e.ctrlKey)) || (e.keyCode == 8 || e.which == 8) || (e.keyCode == 46 || e.which == 46)) {
        submit.disabled = false;
      }
      if ((e.keyCode == 83 && e.ctrlKey) || (e.which == 83 && e.ctrlKey)) {
        e.preventDefault();
        submForm();
      }
    }
    
    textarea.onkeypress = function () {
      submit.disabled = false;
    }

    function submForm() {
      var xmlHttp = new XMLHttpRequest();
      var url = "";
      var parameters = "content=" + textarea.value;

      xmlHttp.open("POST", url, true);
      xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      xmlHttp.setRequestHeader("Cache-Control", "no-cache, max-age=0");
      xmlHttp.setRequestHeader("Pragma", "no-cache");

      xmlHttp.onreadystatechange = function() {
        submit.disabled = (xmlHttp.readyState == 4 && xmlHttp.status == 200) ? true : false;
      }

      xmlHttp.send(parameters);

      return false;
    }
  </script>
</body>
</html>