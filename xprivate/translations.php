<!doctype html>
<html lang="uk_RU">
<head>
  <meta charset="UTF-8">
  <meta
      name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Переводчик интерфейса | Ш++</title>
  <link
      rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
      integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
  <link rel="shortcut icon" href="/img/favicon.png" type="image/png">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <h1 class="mx-auto text-light">Переводчик Ш++</h1>
</nav>
<div class="container">
  <?php $translationPath = dirname(__DIR__) . '/translations/';
  if (!is_writable($translationPath) ||
      !is_writable($translationPath . 'ua.json') ||
      !is_writable($translationPath . 'ru.json') ||
      !is_writable($translationPath . 'en.json')): ?>
    <div class="alert alert-danger mt-2" role="alert">
      <h4 class="center-align">С файлами не все так просто. Возможно у них нет разрешений на запись. Обратитесь к хакеру 😎</h4>
    </div>
  <?php endif; ?>
  <?php session_start();
  if (isset($_SESSION['error']) && $_SESSION['error']): ?>
    <div class="alert alert-danger mt-2">
      <?= $_SESSION['error'] ?>
    </div>
  <?php unset($_SESSION['error']);
  endif;
  if (isset($_SESSION['message']) && $_SESSION['message']): ?>
    <div class="alert alert-success mt-2">
        <?= $_SESSION['message'] ?>
    </div>
      <?php unset($_SESSION['message']);
  endif; ?>
  <div class="alert alert-primary mt-2" role="alert">
    ❗ Старайтесь не делать слишком много изменений. Возможно что-то пойдет не так.
  </div>
  <div class="alert alert-dark mt-2">
    <h5 class="center-align">Спец символы</h5>
    <ul>
      <li>& &mdash; &amp;amp;</li>
      <li>< &mdash; &amp;lt;</li>
      <li>> &mdash; &amp;gt;</li>
      <li>" &mdash; &amp;quot;</li>
      <li>' &mdash; &amp;#039;</li>
      <li>неразрывный пробел &mdash; &amp;nbsp;</li>
      <li>тире &mdash; &amp;mdash;</li>
    </ul>
  </div>
  <ul class="nav nav-tabs nav-justified" role="tablist">
    <li class="nav-item translation_tab" data-lang="ua">
      <a class="nav-link <?= !isset($_GET['tab']) || $_GET['tab'] === 'ua' ? 'active' : ''?>" id="ua-tab" data-toggle="tab" href="#ua" role="tab" aria-controls="ua" aria-expanded="true">🇺🇦 UA</a>
    </li>
    <li class="nav-item translation_tab" data-lang="ru">
      <a class="nav-link <?= (isset($_GET['tab']) && $_GET['tab'] === 'ru') ? 'active' : '' ?>" id="ru-tab" data-toggle="tab" href="#ru" role="tab" aria-controls="ru">🇷🇺 RU</a>
    </li>
    <li class="nav-item translation_tab" data-lang="en">
      <a class="nav-link <?= (isset($_GET['tab']) && $_GET['tab'] === 'en') ? 'active' : '' ?>" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en">🇬🇧 EN</a>
    </li>
  </ul>
  <form class="mb-5" method="post" action="./translations_form_handler.php">
    <div id="translation_form" class="mt-3"></div>
  </form>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
<script>
  $(document).ready(function () {
    const init = function () {
      let initLang = $('.nav-link.active').parent().data('lang');
      populateForm(initLang);
    };
    init();

    window.onpopstate = function (ev) {
      populateForm(ev.state.tab);
      $('.nav-link.active').removeClass('active');
      $(`#${ev.state.tab}-tab`).addClass('active');
    };

    $('.translation_tab').click(function () {
      const langCode = $(this).data('lang');
      history.pushState({tab: langCode}, 'tab_change', `translations.php?tab=${langCode}`);
      populateForm(langCode);
    });

    function populateForm(langCode) {
      fetch(`${location.origin}/translations/${langCode}.json`).then(response => {
        return response.json();
      }).then(translation => {
        let formHtml = `<input type="hidden" value="${langCode}" name="lang">`;
        for (const key in translation) {
          if (!translation.hasOwnProperty(key)) {
            continue;
          }
          const value = translation[key]
              .replace(/&/g, `&amp;`)
              .replace(/</g, `&lt;`)
              .replace(/>/g, `&gt;`)
              .replace(/"/g, `&quot;`)
              .replace(/'/g, `&#039;`);
          formHtml += `<div class="form-group row">
            <label for="input_${key}" class="col-sm-2 col-form-label">${key}</label>
            <div class="col-sm-10">
            <input type="text" class="form-control" id="input_${key}" value="${value}" name="${key}">
            </div>
            </div>`
        }
        formHtml += `<button class="btn btn-primary" type="submit">💾 Сохранить</button>`;
        $('#translation_form').html(formHtml);
      }).catch(err => alert(err));
    }
  });
</script>
</body>
</html>
