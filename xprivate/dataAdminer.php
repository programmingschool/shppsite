<?php
include '../backend/db.php';

if (isset($_POST['action'])) {
    echo 'post undefined';
    return;
}

$action = $_POST['action'];
switch ($action) {
    case 'getAllData':

        $query = "SELECT st.id AS id, st.name AS name, st.age AS age, st.course AS course, st.address AS address, st.phone AS phone, st.parphone AS parphone, st.gmail AS gmail, st.education AS education, st.skype AS skype, st.english AS english, st.skills AS skills, st.dest AS dest, st.photo AS photo, st.sendmail AS sendmail, st.datetime AS datetime, st.portfolio AS portfolio, st.note AS note, student_state.studstate_name AS current_state, st.interview AS interview  FROM students AS st LEFT JOIN student_state ON st.current_state_id = student_state.studstate_id";
        $result = mysqli_query($con, $query) || die('Couldn t execute query.' . mysqli_error($con) . 'query: ' . $query);

        $students = array();

        while ($row = mysqli_fetch_assoc($result)) {
            array_push($students, $row);
        }
        $response->students = $students;

        //get student_state_value
        $query = "SELECT * FROM student_state";
        $result = mysqli_query($con, $query) || die("Couldn t execute query." . mysqli_error($con) . "query: " . $query);

        $student_state_value = array();

        while ($row = mysqli_fetch_assoc($result)) {
            $student_state_value[$row['studstate_id']] = $row['studstate_name'];
        }
        $response->student_state_value = $student_state_value;

        echo json_encode($response);
        break;

    case 'getData':
        $page = $_POST['page']; // get the requested page
        $limit = $_POST['rows']; // get how many rows we want to have into the grid
        $sidx = $_POST['sidx']; // get index row - i.e. user click to sort
        $sord = $_POST['sord']; // get the direction
        if (!$sidx) $sidx = 1;

        $result = mysqli_query($con, "SELECT COUNT(*) AS count FROM students") || die("Couldn`t count students. " . mysqli_error($con));
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        $count = $row['count'];

        if ($count > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages) {
            $page = $total_pages;
        }
        $start = $limit * $page - $limit; // do not put $limit*($page - 1)

        $query = "SELECT * FROM students ORDER BY $sidx $sord LIMIT $start , $limit";
        $result = mysqli_query($con, $query) || die("Couldn t execute query." . mysqli_error($con) . "query: " . $query);

        $response->page = $page;
        $response->total = $total_pages;
        $response->records = $count;

        $i = 0;
        while ($row = mysqli_fetch_assoc($result)) {
            $response->rows[$i]['id'] = $row[id];
            $response->rows[$i]['cell'] = $row;
            $response->rows[$i]['cell']['db_id'] = $row[id];
            $i++;
        }
        echo json_encode($response);
        break;
    case 'update':
        $result = false;
        $query = 'null';

        if (count($_POST['data']) > 0 && $_POST['id']) {
            $data = $_POST['data'];
            $updates = [];
            foreach ($data as $key => $value) {
                $col_name = mysqli_real_escape_string($con, $key);
                $expr = mysqli_real_escape_string($con, htmlspecialchars(strip_tags($value), ENT_QUOTES));

                $expr = "'$expr'";
                $updates[] = "$col_name = $expr";
            }
            $implodeArray = implode(', ', $updates);
            $query = "UPDATE `students` SET $implodeArray  WHERE id=" . $_POST['id'] . ";";
            $result = mysqli_query($con, $query);
        }
        echo $result;
        break;
    default:
        echo 'action undefined';
}
