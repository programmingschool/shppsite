var app = angular.module('app', ['ngRoute']);
app.config(['$routeProvider','$locationProvider',
  function($routeProvider,$locationProvider) {
    $routeProvider.
      when('/', {
        templateUrl: 'pages/first-form.html',
        controller: 'first'
      }).
      when('/check', {
        templateUrl: 'pages/check-school-form.html',
        controller: 'check'
      }).
      when('/course', {
        templateUrl: 'pages/course-form.html',
        controller: 'course'
      }).
      when('/main', {
        templateUrl: 'pages/main-form.html',
        controller: 'main'
      }).
      when('/main-school', {
        templateUrl: 'pages/main-school-form.html',
        controller: 'main-school'
      }).
      when('/price', {
        templateUrl: 'pages/price-form.html',
        controller: 'price'
      }).
      when('/last', {
        templateUrl: 'pages/last-form.html',
        controller: 'last'
      }).
      otherwise({
        redirectTo: '/'
      });
      $locationProvider.html5Mode(true).hashPrefix('!');
  }
]);
app.controller('mainController',function($scope) {
});
app.controller('first',function($scope) {
});
app.controller('check',function($scope) {
});
app.controller('course',function($scope) {
});
app.controller('main',function($scope) {
});
app.controller('main-school',function($scope) {
});
app.controller('price',function($scope) {
});
app.controller('last',function($scope) {
});