'use strict';
var app = angular.module('app', [
  'ngRoute', 'ngTouch', 'duScroll', 'ngSanitize', 'angularFileUpload', 'ngResource',
  'ngCookies', 'infinite-scroll'
]);

app.config(['$routeProvider', '$locationProvider',
  function ($routeProvider, $locationProvider) {
    $routeProvider.when('/', {
      templateUrl: 'pages/home.html?v=' + siteVer,
      controller: 'home'
    }).when('/en', {
      templateUrl: 'pages/home.html?v=' + siteVer,
      controller: 'home',
      directlang: 'en'
    }).when('/ru', {
      templateUrl: 'pages/home.html?v=' + siteVer,
      controller: 'home',
      directlang: 'ru'
    }).when('/ua', {
      templateUrl: 'pages/home.html?v=' + siteVer,
      controller: 'home',
      directlang: 'ua'
    }).when('/:lang?/home', {
      templateUrl: 'pages/home.html?v=' + siteVer,
      controller: 'home'
    }).when('/:lang?/news', {
      templateUrl: 'pages/news.html?v=' + siteVer,
      controller: 'news'
    }).when('/:lang?/courses', {
      templateUrl: 'pages/courses.html?v=' + siteVer,
      controller: 'courses'
    }).when('/:lang?/potential', {
      templateUrl: 'pages/potential.html?v=' + siteVer,
      controller: 'potential'
    }).when('/:lang?/about', {
      templateUrl: 'pages/about.html?v=' + siteVer,
      controller: 'about'
    }).when('/:lang?/anketa', {
      templateUrl: 'pages/first-form.html?v=' + siteVer,
      controller: 'first'
    }).when('/:lang?/check', {
      templateUrl: 'pages/check-school-form.html?v=' + siteVer,
      controller: 'check'
    }).when('/:lang?/choose', {
      templateUrl: 'pages/course-form.html?v=' + siteVer,
      controller: 'course'
    }).when('/:lang?/main', {
      templateUrl: 'pages/main-form.html?v=' + siteVer,
      controller: 'main'
    }).when('/:lang?/main-school', {
      templateUrl: 'pages/main-school-form.html?v=' + siteVer,
      controller: 'main'
    }).when('/:lang?/last', {
      templateUrl: 'pages/last-form.html?v=' + siteVer,
      controller: 'last'
    }).when('/:lang?/camera', {
      templateUrl: 'pages/camera.html?v=' + siteVer,
      controller: 'camera'
    }).when('/:lang?/supportus', {
      templateUrl: 'pages/support.html?v=' + siteVer
    }).otherwise({
      redirectTo: '/'
    });
    $locationProvider.html5Mode(true).hashPrefix('!');
  }
]);

app.run(function ($rootScope, $location, $anchorScroll, $routeParams, $timeout) {
  // if(window.FastClick) {
  //   FastClick.attach(document.body);
  // }
  $rootScope.$on('$routeChangeSuccess', function () {
    $timeout(function () {
      $anchorScroll();
    }, 700);
  })
});

app.service('translationService', function ($resource) {
  this.getTranslation = function ($scope, language) {
    var languageFilePath = 'translations/' + language + '.json?v=' + siteVer;
    $resource(languageFilePath).get(function (data) {
      $scope.translation = data;
      $scope.translationCallback();
    });
  }
});

app.service('$location_skip', ['$location', '$route', '$rootScope', function ($location, $route, $rootScope) {
  $location.skipReload = function () {
    var lastRoute = $route.current;
    var un = $rootScope.$on('$locationChangeSuccess', function () {
      $route.current = lastRoute;
      un();
    });
    return $location;
  };
  return $location;
}]);

app.service('getConfigurations', function ($http) {
  return function ($scope) {
    if (!$scope.configurations) {
      $http.get('configs.json')
          .success(function (data) {
            if (typeof data === 'object')
              $scope.configurations = data;
            else {
              $scope.configurations = {'camera': 'off'}
            }
          })
          .error(function () {
            $scope.configurations = {'camera': 'off'}
          });
    }
  }
});

app.directive("scroll", function ($window) {
  return function (scope, element, attrs) {
    angular.element($window).bind("scroll", function () {
      if (this.pageYOffset >= 500)
        angular.element('#scrollup').fadeIn('slow');
      else
        angular.element('#scrollup').fadeOut('slow');
      scope.$apply();
    });
  }
});

app.directive('newsSlider', function ($timeout) {
  return function (scope, elem, attrs) {
    var obj = JSON.parse(attrs.check);
    $timeout(function () {
      if (Object.keys(obj).length > 1) {
        elem.addClass('slicked');
        elem.slick({
          lazyLoad: 'ondemand',
          infinite: true,
          pauseOnHover: true,
          autoplay: false,
          autoplaySpeed: 5000,
          speed: 300,
          arrows: true,
          prevArrow: '<button type="button" class="slick-prev">Previous</button>',
          nextArrow: '<button type="button" class="slick-next">Next</button>',
        });
        elem.click(function (e) {
          var ratio = e.offsetX / e.currentTarget.clientWidth;
          if (ratio < 0.25) {
            angular.element(this).slick("slickPrev");
          }
          else {
            angular.element(this).slick("slickNext");
          }
        });

        elem.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
          if (currentSlide == nextSlide) {
            return;
          }
          var images = angular.element(event.currentTarget).find("div.slick-slide").not(".slick-cloned").find('img');
          var nextimage = angular.element(images[nextSlide + 1])[0];
          if (nextimage && nextimage.src == "") {
            nextimage.src = nextimage.dataset.lazy;
            nextimage.removeAttribute("data-lazy");
          }
        });
      }
      else {
        var childImg = angular.element(elem).children().children();
        var childImgDataLazy = childImg.attr('data-lazy');
        if (childImgDataLazy != "") {
          childImg.attr('src', childImgDataLazy)
        }
      }
    }, 0);
  }
});

app.directive('partnersSlider', function () {

  return function (scope, elem) {
      elem.owlCarousel({
          loop: true,
          margin: 10,
          nav: false,
          dots: false,
          autoplay: true,
          autoplaySpeed: 500,
          autoplayTimeout: 3000,
          autoplayHoverPause: true,
          responsive: {
              0: {items: 1},
              600: {items: 3},
              1024: {items: 4},
              1200: {items: 5}
          }
      });
  }
});

app.directive('mainSlider', function ($timeout) {
  return function (scope, elem, attrs) {
    $timeout(function () {
      elem.owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        items: 1,
        autoplayHoverPause: true,
        responsive: {
          0: {
            autoplay: false
          },
          770: {
            autoplay: true,
            autoplaySpeed: 500,
            autoplayTimeout: 5000,
            autoplayHoverPause: true
          }
        }
      });
      elem.click(function (e) {
        if (e.target.className === "owl-next" || e.target.className === "owl-prev" ||
            e.target.className === "owl-stage" || e.target.localName === "a")
          return;
        var ratio = e.clientX / e.currentTarget.clientWidth;
        if (ratio < 0.2) {
          angular.element(this).trigger('prev.owl.carousel');
        }
        else {
          angular.element(this).trigger('next.owl.carousel');
        }
      })
    })
  }
});

app.directive('aboutSlider', function ($timeout) {
  return function (scope, elem) {
    $timeout(function () {
      elem.owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        items: 1,
        responsive: {
          0: {
            autoplay: false
          },
          770: {
            autoplay: true,
            autoplaySpeed: 500,
            autoplayTimeout: 5000,
            autoplayHoverPause: true
          }
        }
      });
      elem.click(function (e) {
        if (e.target.className == "owl-stage") {
          return;
        }
        var ratio = e.offsetX / e.currentTarget.clientWidth;
        if (ratio < 0.25) {
          angular.element(this).trigger('prev.owl.carousel');
        }
        else {
          angular.element(this).trigger('next.owl.carousel');
        }
      })
    })
  }
});

app.directive('potentialSlider', function ($timeout) {
  return function (scope, elem, attrs) {
    var number = 0;
    var bgColors = ['rgb(26, 188, 156)', 'rgb(46, 204, 113)', 'rgb(241, 196, 15)', 'rgb(52, 152, 219)', 'rgb(211, 84, 0)', 'rgb(155, 89, 182)', 'rgb(231, 76, 60)', 'rgb(230, 126, 34)'];
    $timeout(function () {
      elem.owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        items: 1,
        dots: false,
        onTranslate: function () {
          elem.css({'backgroundColor': bgColors[number]});
          number = (number < 7) ? number + 1 : 0;
        },
        responsive: {
          0: {
            autoplay: false
          },
          768: {
            autoplay: true,
            autoplaySpeed: 1000,
            autoplayTimeout: 6000,
            autoplayHoverPause: true
          }
        }
      });
      elem.click(function (e) {
        if (e.target.className === 'owl-next' || e.target.className === 'owl-prev' || e.target.className === 'owl-stage') {
          return;
        }
        var ratio = e.clientX / e.currentTarget.clientWidth;
        if (ratio < 0.20) {
          angular.element(this).trigger('prev.owl.carousel');
        }
        else {
          angular.element(this).trigger('next.owl.carousel');
        }
      })
    })
  }
});

app.directive('ngThumb', ['$window', function ($window) {
  var helper = {
    support: !!($window.FileReader && $window.CanvasRenderingContext2D),
    isFile: function (item) {
      return angular.isObject(item) && item instanceof $window.File;
    },
    isImage: function (file) {
      var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
      return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
    }
  };

  return {
    restrict: 'A',
    template: '<canvas/>',
    link: function (scope, element, attributes) {
      if (!helper.support) return;

      var params = scope.$eval(attributes.ngThumb);

      if (!helper.isFile(params.file)) return;
      if (!helper.isImage(params.file)) return;

      var canvas = element.find('canvas');
      var reader = new FileReader();

      reader.onload = onLoadFile;
      reader.readAsDataURL(params.file);

      function onLoadFile(event) {
        var img = new Image();
        img.onload = onLoadImage;
        img.src = event.target.result;
      }

      function onLoadImage() {
        var width = params.width || this.width / this.height * params.height;
        var height = params.height || this.height / this.width * params.width;
        canvas.attr({width: width, height: height});
        canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
      }
    }
  }
}]);

app.directive('ngCamera', function ($timeout) {

  return function (scope) {

    $timeout(function () {
      if (scope.configurations && scope.configurations.camera === "off")
        return;

      var showInfo = function (msg, type) {
        var infoElem = angular.element('#status_con');
        type = type || 'success';
        if (type === 'success') {
          infoElem.removeClass("red").addClass("green");
        }
        else {
          infoElem.removeClass("green").addClass("red");
        }
        infoElem.html(msg);
      };

      $.getScript('js/socket.io.js')
          .done(function () {
            $(document).ready(function () {
              $(".video-content").css({visibility: "visible"});

              var m3u8 = "http://shpp.me:1935/live-ll/camera/playlist.m3u8";

              if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                var e = document.getElementById("video");
                e.innerHTML = '<video width="640" height="480" src="' + m3u8 + '" controls="controls" autoplay="autoplay" ></video>';
                e.style.display = 'block';
                showInfo(scope.translation.camera_phoneInfo);
                show([$("#status_con_div")]);
                $(".playeroverlay").hide();
              } else {
                startVideoPlayer();
                var joystick = startJoystick({
                  "canvas": "joystick",
                  "canvas_div": "playerbox",
                  "colorBorder": {r: 220, g: 220, b: 220},
                  "colorCursor": {r: 180, g: 180, b: 180},
                  "colorCursorLine": {r: 150, g: 150, b: 150},
                  "alphaCursor": 220,
                  "alphaBorder": 255,
                  "alphaLineCursor": 50,
                  "startX": 50,
                  "startY": 50,
                  "padX": 4,
                  "padY": 3,
                  "iskrl": 10,
                  "joyWidth": 100,
                  "joyHeight": 50,
                  "cameraHeight": 1 / 2,
                  onCommand: function (x, y) {
                    x = 100 - x;
                    sendToServerLeft(~~(x));
                    sendToServerTop(~~(y));
                  },
                  onLeave: function () {
                    var $joystick = $("#joystick");
                    $joystick.stop();
                    $joystick.css({opacity: 1});
                    $joystick.animate({opacity: 0.4, duration: 5});
                  },
                  onEnter: function () {
                    var $joystick = $("#joystick");
                    $joystick.stop();
                    $joystick.animate({opacity: 1, duration: 5});
                  }
                });

                $("#btnControl").click(function () {
                  showInfo(scope.translation.camera_getControl);
                  sendToServer("Want to control");
                });

                hide([$("#joystick")]);

                var client = startClient(io, {
                  onConnect: function () {
                    show([$("#btnControl")]);
                    show([$("#status_con_div")]);
                    showInfo(scope.translation.camera_onConnect);
                    if (joystick != null) {
                      joystick.draw(50, 50);
                    }
                  },
                  onDisconnect: function () {
                    showInfo(scope.translation.camera_onDisconnect, "error");
                    show([$("#status_con_div")]);
                    hide([$("#bar_div"), $("#btnControl"), $("#joystick")]);
                  },
                  onError: function () {
                    showInfo(scope.translation.camera_onDisconnect, "error");
                    show([$("#status_con_div")]);
                  },
                  onMessage: function (msg) {
                    if (msg === "Limited control") {
                      showInfo(scope.translation.camera_limControl);
                      animateProgressBar($("#bar"), $("#bar_div"), progressBarTime);
                      show([$("#joystick")]);
                      hide([$("#btnControl")]);
                    } else if (msg === "Switch to limited control") {
                      showInfo(scope.translation.camera_limControl);
                      animateProgressBar($("#bar"), $("#bar_div"), progressBarTime);
                    } else if (msg === "Switch to unlimited control") {
                      showInfo(scope.translation.camera_unlimControl);
                      hide([$("#bar_div")]);
                    } else if (msg === "Unlimited control") {
                      showInfo(scope.translation.camera_unlimControl);
                      show([$("#joystick")]);
                      hide([$("#bar_div"), $("#btnControl")]);
                    } else if (msg === "End of control") {
                      showInfo(scope.translation.camera_endControl);
                      hide([$("#bar_div"), $("#joystick")]);
                      show([$("#btnControl")]);
                    } else if (msg.match(/waiting queue/)) {
                      var queueNumber = parseInt(msg.replace(/waiting queue /, ""));
                      animateProgressBar($("#bar"), $("#bar_div"), parseInt(queueNumber) * progressBarTime);
                      showInfo(scope.translation.camera_waitQueue.replace(/<!-- time -->/, queueNumber));
                      hide([$("#btnControl"), $("#joystick")]);

                    } else if (msg.match(/queue number/)) {
                      var queueNumber = parseInt(msg.replace(/queue number /, ""));
                      showInfo(scope.translation.camera_waitQueue.replace(/<!-- time -->/, queueNumber));
                    }
                  }
                });
                var sendToServer = function (msg) {
                  // console.log("I:      " + msg);
                  client.sendMessage(msg);
                };

                var sendToServerLeft = function (angle) {
                  client.sendAngleLeft(angle)
                };
                var sendToServerTop = function (angle) {
                  client.sendAngleTop(angle)
                };
              }
            })
          })
          .fail(function () {
            showInfo(scope.translation.camera_onDisconnect, "error");
            $("#status_con_div").show();
          })
    })
  }
});
