'use strict';

app.controller('mainController', ['getConfigurations', '$scope', '$location', '$anchorScroll', '$document', '$timeout', '$http', '$sce', '$cookieStore', 'translationService', '$routeParams', '$route', '$location_skip', function (getConfigurations, $scope, $location, $anchorScroll, $document, $timeout, $http, $sce, $cookieStore, translationService, $routeParams, $route, $location_skip) {
  $scope.setIteminLS = function (itemKey, itemVal) {
    if (itemVal === undefined) {
      localStorage.removeItem(itemKey);
    } else {
      localStorage.setItem(itemKey, itemVal);
    }
  };

  getConfigurations($scope);
  // translation module

  var langRe = /(\/en|\/ru|\/ua)/;
  var locationlang = langRe.exec($location.path());
  if (locationlang !== null) {
    $scope.selectedLanguage = locationlang[0].replace("/", "");
  } else if ($cookieStore.get('lang') !== undefined) {
    $scope.selectedLanguage = $cookieStore.get('lang');
  } else {
    $scope.selectedLanguage = 'ru';
  }

  $scope.translate = function () {
    translationService.getTranslation($scope, $scope.selectedLanguage);
    document.cookie = "lang=%22" + $scope.selectedLanguage + "%22";
    var urllang = langRe.exec($location.path());

    if (urllang == null) {
      $timeout(function () {
        var new_location = "/" + $scope.selectedLanguage + $location.path();
        $location_skip.skipReload().path(new_location);
      })
    }
    else {
      if (urllang[0] !== "/" + $scope.selectedLanguage) {
        $timeout(function () {
          var new_location = $location.path().replace(langRe, "/" + $scope.selectedLanguage);
          $location_skip.skipReload().path(new_location);
        })
      }
    }

  };
  $scope.translationCallback = function () {
    // empty function
  };
  $scope.redefineTranslationCallback = function (func) {
    $scope.translationCallback = func;
  };


  // $scope.$on('$routeChangeSuccess', function() {
  //     if ($route.current.$$route.directlang!==undefined) {
  //         var directlang = $route.current.$$route.directlang;
  //         $routeParams.lang = directlang;
  //     }

  //     if($routeParams.lang!==undefined){
  //         var lang = '';
  //         switch ($routeParams.lang) {
  //             case "en":
  //                 lang = "en";
  //                 break;
  //             case "ru":
  //                 lang = "ru";
  //                 break;
  //             case "ua":
  //                 lang = "ua";
  //                 break;
  //             default:
  //             if($cookieStore.get('lang')!==undefined){
  //                 lang = $cookieStore.get('lang');
  //             }
  //             else{
  //                 lang = $scope.selectedLanguage;
  //             }
  //         }
  //         if ($scope.selectedLanguage != lang) {
  //             $scope.selectedLanguage = lang;
  //             $scope.translate();
  //         }
  //     }
  // });


  //forced change of language with location.search()
  /*var searchObject = $location.search();
  if(searchObject.hasOwnProperty('lang')){
      var lang = '';
      switch (searchObject.lang) {
          case "en":
              lang = "en";
              break;
          case "ru":
              lang = "ru";
              break;
          case "ua":
              lang = "ua";
              break;
          default:
              lang = "ru";
      }
      $scope.selectedLanguage = lang;
      $location.search('lang', null);
  }*/
  // end of [forced change of language]

  $scope.translate();
  // ------------------
  $scope.isActiveLang = function (lang) {
    return lang === $scope.selectedLanguage;
  };
  $scope.isActive = function (route) {
    return ('/' + $scope.selectedLanguage + route) === $location.path() || route === $location.path();
  };
  $scope.toTheTop = function () {
    $document.scrollTop(0, 500);
  };
  $scope.SkipValidation = function (value) {
    return $sce.trustAsHtml(value);
  };
  $scope.trustAsResourceUrl = function (value) {
    return $sce.trustAsResourceUrl(value);
  };
  $scope.dateView = function (date) {
    var monthes = {
      en: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      ru: ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"],
      ua: ["січня", "лютого", "березня", "квітня", "травня", "червня", "липня", "серпня", "вересня", "жовтня", "листопада", "грудня"]
    };
    var years = {
      en: "",
      ru: "г.",
      ua: "р."
    };
    date = date.split("-");
    var day = date['2'];
    var month = parseInt(date['1']) - 1;
    var year = date['0'];

    date = day + " " + monthes[$scope.selectedLanguage][month] + " " + year + years[$scope.selectedLanguage];
    return date;
  };
  $scope.sendMail = function () {
    var selector = angular.element('.mail-text textarea');
    var mailtext = selector.val();

    if (mailtext === '') {
      selector.attr("placeholder", $scope.translation.writesmth).focus();
    } else if (!mailtext.match($scope.regComment)) {
      angular.element(selector).css({'border': '2px solid rgb(231,76,60)'});
    } else {
      mailtext = mailtext.replace(/script/g, ' ').replace(/document/g, ' ');
      $http({
        method: 'POST',
        url: 'backend/saveData.php',
        data: $.param({mailtext: mailtext}),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (data) {
        selector.val('').attr("placeholder", $scope.translation.thanksForLetter);
        // angular.element(".mailUs .mail-text").hide();
        // angular.element(".mailUs .thanksForLetter").show();
      });
    }
  };

  angular.element(document).ready(function () {
    $timeout(function () {
      angular.element('footer').css({'visibility': 'visible'});
    }, 700);
    angular.element('.mail-text textarea').focus(function () {
      angular.element(this).css({'border': '2px solid white'});
    });
    angular.element('.ask, .submenu-list li a').click(function () {
      $anchorScroll();
    });
  });

  $scope.student = {
    name: '',
    surname: '',
    birthday: '',
    studyform: '',
    nickname: 'test',
    about: '',
    city: 'Кропивницкий',
    phone: '',
    parent_phone: '',
    parent_name: '',
    email: '',
    lessontime: '',
    hours: '',
    portfolio: '',
    occupation: '',
    english: '',
    photo: '',
    course: ''
  };
  $scope.birthday = {
    day: '',
    month: '',
    year: '',
    monthes: {
      'ua': [
        {id: '00', name: 'Січень'},
        {id: '01', name: 'Лютий'},
        {id: '02', name: 'Березень'},
        {id: '03', name: 'Квітень'},
        {id: '04', name: 'Травень'},
        {id: '05', name: 'Червень'},
        {id: '06', name: 'Липень'},
        {id: '07', name: 'Серпень'},
        {id: '08', name: 'Вересень'},
        {id: '09', name: 'Жовтень'},
        {id: '10', name: 'Листопад'},
        {id: '11', name: 'Грудень'}
      ],
      'ru': [
        {id: '00', name: 'Январь'},
        {id: '01', name: 'Февраль'},
        {id: '02', name: 'Март'},
        {id: '03', name: 'Апрель'},
        {id: '04', name: 'Май'},
        {id: '05', name: 'Июнь'},
        {id: '06', name: 'Июль'},
        {id: '07', name: 'Август'},
        {id: '08', name: 'Сентябрь'},
        {id: '09', name: 'Октябрь'},
        {id: '10', name: 'Ноябрь'},
        {id: '11', name: 'Декабрь'}
      ],
      'en': [
        {id: '00', name: 'January'},
        {id: '01', name: 'February'},
        {id: '02', name: 'March'},
        {id: '03', name: 'April'},
        {id: '04', name: 'May'},
        {id: '05', name: 'June'},
        {id: '06', name: 'July'},
        {id: '07', name: 'August'},
        {id: '08', name: 'September'},
        {id: '09', name: 'October'},
        {id: '10', name: 'November'},
        {id: '11', name: 'December'}
      ]
    }

  };
  $scope.checkIfNameAgeEmpty = function () {
    return $scope.student.name === '' || $scope.student.surname === '' || $scope.student.birthday === '';
  };
  $scope.dayPattern = /^0?[1-9]|[12]\d|3[01]$/;
  $scope.yearPattern = /^(19\d{2}|20(0\d|10))$/;
  $scope.agePattern = /^[0-9]+$/;
  $scope.birthdayPattern = /^(19\d{2}|20(0\d|1[1-5]))-[01]\d-[0123]\d$/;
  $scope.gmailPattern = /^.+@gmail.com$/;
  $scope.phonePattern = /^[0-9\s\-\+\,\;]+$/;
  $scope.skypePattern = /^[a-zA-Z][\@\w\.,\-]{5,31}$/;
  $scope.emailPattern = /^.+@[\w]+\.[\w]+$/;
  $scope.regAge = /^[\wа-яА-ЯёЁіІїЇєЄґҐ\s]+$/;
  $scope.addressPattern = /^[\wа-яА-ЯёЁіІїЇєЄґҐ\'\"\s\-\.\,\:\№\/\\]+$/;
  $scope.regComment = /^[\w\'\"?!()\@\:\;\.\,\/\+\№а-яА-ЯёЁіІїЇєЄґҐ\s\-]*$/;
  $scope.namePattern = /^[\w\'\"а-яА-ЯёЁіІїЇєЄґҐ\-\.\,\:]+/;
  $scope.surnamePattern = /^[\w\'\"а-яА-ЯёЁіІїЇєЄґҐ\-\.\,\:]+/;
  $scope.portfolioPattern = /^[\w\'\"?!()\@\:\;\.\,\/\+\№а-яА-ЯёЁіІїЇєЄґҐ\&\@\#\\%\?\=\~\s\-]*$/;
}]);

app.controller('home', ['$scope', '$timeout', '$location', '$routeParams', function ($scope, $timeout, $location, $routeParams) {
  $scope.sameHeight = function (selector) {
    var elements = document.querySelectorAll(selector),
        max_height = 0,
        len = 0,
        i;
    if ((elements) && (elements.length > 0)) {
      len = elements.length;

      for (i = 0; i < len; i++) { // get max height
        elements[i].style.height = ''; // reset height attr
        if (elements[i].clientHeight > max_height) {
          max_height = elements[i].clientHeight;
        }
      }

      for (i = 0; i < len; i++) { // set max height to all elements
        elements[i].style.height = max_height + 'px';
      }
    }
  };
  var init = function () {
    $timeout(function () {
      // $scope.sameHeight('.info-comments');
      $scope.sameHeight('.one-info-news');
    });
    $timeout(function () {
      var feedbacks = document.querySelectorAll(".info-comments");
      var feedbacksArray = []
      for (var i = feedbacks.length; i--; feedbacksArray.unshift(feedbacks[i])) ;

      while (feedbacksArray.length > 4) {
        var randomIndex = ~~(Math.random() * feedbacks.length);
        feedbacksArray.splice(randomIndex, 1);
      }

      var feedbacksContainer = document.getElementById("feedbacks")
      feedbacksContainer.innerHTML = "";
      feedbacksArray.forEach(function (n) {
        feedbacksContainer.appendChild(n)
      });

    });
    window.onresize = function () {
      // $scope.sameHeight('.info-comments');
      $scope.sameHeight('.one-info-news');
    };
  };
  init();
  // redefining funciton translate() from home controller
  $scope.redefineTranslationCallback(function () {
    $timeout(function () {
      angular.element(".owl-item.cloned .codeweek-slide").html(angular.element(".owl-item:not(.cloned) .codeweek-slide").html());
      // angular.element(".owl-item.cloned .first-slide").html(angular.element(".owl-item:not(.cloned) .first-slide").html());
      // angular.element(".owl-item.cloned .fourth-slide").html(angular.element(".owl-item:not(.cloned) .fourth-slide").html())
      // angular.element(".owl-item.cloned .zero-slide").html(angular.element(".owl-item:not(.cloned) .zero-slide").html())
      // angular.element(".owl-item.cloned .second-slide").html(angular.element(".owl-item:not(.cloned) .second-slide").html())
      angular.element(".owl-item.cloned .third-slide").html(angular.element(".owl-item:not(.cloned) .third-slide").html());
      init();
    });
  });


}]);

app.controller('about', function ($scope, $location, $timeout) {
  angular.element(document).ready(function () {
    angular.element('.green-submenu a').click(function () {
      var hash = angular.element(this).attr('href').replace("#", "");
      $location.hash(hash);
    });

    var origin_weare = {
      "people": [
        // {
        //   "name": "sasha",
        //   "img": "img/people/sasha.png",
        //   "position": 'Software engineer'
        // },
        {
          "name": "vova",
          "img": "img/people/vova.jpg",
          "position": 'Web developer'
        },
        {
          "name": "anton",
          "img": "img/people/anton.jpg",
          "position": "Web developer"
        },
        {
          "name": "artem",
          "img": "img/people/artem.jpg",
          "position": "Web developer"
        },
        {
          "name": "lesha",
          "img": "img/people/lesha.jpg",
          "position": "Highload"
        },
        {
          "name": "anatoliy",
          "img": "img/people/anatoliy.png",
          "position": "School founder"
        },
        {
          "name": "igor",
          "img": "img/people/igor.jpg",
          "position": "Android developer"
        },
        // {
        //   "name": "sasha2",
        //   "img": "img/people/sasha2.jpg",
        //   "position": "iOS developer"
        // },
        {
          "name": "nastya",
          "img": "img/people/nastya.jpg",
          "position": "Frontend"
        },
        {
          "name": "roma",
          "img": "img/people/roma.jpg",
          "position": "Streaming media"
        },
        {
          "name": "igor2",
          "img": "img/people/igor2.png",
          "position": "Android developer"
        },
        // {
        //   "name": "dima",
        //   "img": "img/people/dima.jpg",
        //   "position": "Mac developer"
        // },
        {
          "name": "dima2",
          "img": "img/people/dima2.jpg",
          "position": "Android developer"
        },
        // {
        //   "name": "julia",
        //   "img": "img/people/julia.jpg",
        //   "position": "English"
        // },
        // {
        //   "name": "sasha3",
        //   "img": "img/people/sasha3.jpg",
        //   "position": "Android developer"
        // },
        // {
        //   "name": "konstantin",
        //   "img": "img/people/konstantin.jpg",
        //   "position": "Web developer"
        // },
        // {
        //   "name": "eugene",
        //   "img": "img/people/eugene.jpg",
        //   "position": "Nodejs"
        // },
        {
          "name": "igor3",
          "img": "img/people/igor3.jpg",
          "position": "Web developer"
        },
        {
          "name": "ira",
          "img": "img/people/ira.jpg",
          "position": "Manager"
        },
        {
          "name": "maryna",
          "img": "img/people/maryna.png",
          "position": "Volunteer"
        },
        {
          "name": "bogdan",
          "img": "img/people/bogdan.png",
          "position": "Android developer"
        },
        {
          "name": "yuriy",
          "img": "img/people/yuriy.png",
          "position": "Web developer"
        },
        {
          "name": "katya",
          "img": "img/people/katya.jpg",
          "position": "Frontend"
        },
        {
          "name": "igor_pok",
          "img": "img/people/igor_pok.png",
          "position": "Android developer"
        },
        {
          "name": "ira_shk",
          "img": "img/people/ira_shk.png",
          "position": "PHP Laravel"
        },
        {
          "name": "dima_erm",
          "img": "img/people/dima_erm.png",
          "position": "Mentor scratch"
        },
        {
          "name": "valentyn",
          "img": "img/people/valentyn.png",
          "position": "Android developer"
        },
        {
          "name": "anatoliy_she",
          "img": "img/people/anatoliy_she.jpg",
          "position": "Volunteer"
        },
        {
          "name": "andrey_chu",
          "img": "img/people/andrey_chu.jpg",
          "position": "Node.js PHP"
        },
        {
          "name": "roma_tka",
          "img": "img/people/roma_tka.png",
          "position": "Linux"
        },
        {
          "name": "ruslan",
          "img": "img/people/ruslan.png",
          "position": "Embedded"
        },
        {
          "name": "tania",
          "img": "img/people/tania.jpg",
          "position": "Volunteer"
        },
        {
          "name": "artem_shk",
          "img": "img/people/artem_shk.jpg",
          "position": "Volunteer"
        },
        {
          "name": "kostya_rus",
          "img": "img/people/kostya_rus.jpg",
          "position": "Web developer"
        },
      ]
    };

    //only for 2rows variant
    /*if(origin_weare.people.length%2){
        origin_weare.people.push({"name": "anonimous", "img": "img/people/anonymous.svg","position": "Developer"});
    }*/

    //randomize
    origin_weare.people.sort(function () {
      return 0.5 - Math.random()
    });
    var random_weare = JSON.stringify(origin_weare);

    var translate_peopledata = function () {

      var weare = JSON.parse(random_weare);
      weare.people.forEach(function (person, i) {
        if ($scope.translation && $scope.translation[person.name]) {
          person.name = $scope.translation[person.name];
        }
      });

      $scope.weare = weare;
    };

    translate_peopledata();

    // redefining funciton translate() from potential controller
    $scope.redefineTranslationCallback(function () {
      if ($location.$$path.match(/\/about/g)) {
        $timeout(function () {
          angular.element(".owl-item.cloned .first-slide-about").html(angular.element(".owl-item:not(.cloned) .first-slide-about").html());
          angular.element(".owl-item.cloned .second-slide-about").html(angular.element(".owl-item:not(.cloned) .second-slide-about").html());
          angular.element(".owl-item.cloned .third-slide-about").html(angular.element(".owl-item:not(.cloned) .third-slide-about").html());
          translate_peopledata();
        })
      }
    })
  })
});

app.controller('news', function ($scope, $http, $window, $timeout, $location, News) {
  $scope.newsRequested = false;
  angular.element(document).ready(function () {
    $scope.news = new News();
    $scope.newsRequested = true;

    angular.element($window).bind('resize', function () {
      $scope.checkWindowSize();
    });

    $scope.showMore = function (index) {
      angular.element('.row' + index + " .news-content .content").css({"height": "auto"});
      angular.element('.row' + index).addClass('news-open');
      $scope.news.items[index]['content'] = $scope.news.items[index]['realcontent'];
    };
    $scope.closeMore = function (index) {
      angular.element('.row' + index).removeClass('news-open');
      $scope.news.items[index]['content'] = $scope.news.items[index]['shortcontent'];
      angular.element('.row' + index + ' .slicked').slick("unslick").slick({
        infinite: true,
        speed: 300,
        arrows: true,
        prevArrow: '<button type="button" class="slick-prev">Previous</button>',
        nextArrow: '<button type="button" class="slick-next">Next</button>',
      });
    };
    // redefining funciton translate() from news controller
    $scope.redefineTranslationCallback(function () {
      if ($location.$$path.match("\/news") != null && $location.$$path.match("\/news")[0] !== "/news") {
        return;
      }

      for (var i = 0; i < $scope.news.items.length; i++) {
        $scope.news.items[i].realcontent = $scope.news.items[i]['content_' + $scope.selectedLanguage];
        $scope.news.items[i].title = $scope.news.items[i]['title_' + $scope.selectedLanguage];
      }
      $scope.checkWindowSize(true);
    });
  });
  $scope.cutContent = function () {
    var header = angular.element(".news-content h2");
    var headerHeight = header.height() + parseInt(header.css("margin-top")) + parseInt(header.css("margin-bottom"));
    var currentWidth = Math.round(angular.element(".news-row").width() * 0.5 * 0.9);
    for (var i = 0; i < $scope.news.items.length; i++) {
      var currentHeight = Math.floor(currentWidth / 0.9 * $scope.news.items[i]['ratio'] - headerHeight - 25);
      var builder = angular.element("#builder");
      builder.css({"width": currentWidth}).html($scope.news.items[i].realcontent);

      var wasCutted = false;
      var cut = function () {
        if (builder.outerHeight() > currentHeight) {
          builder.html(function (index, text) {
            return text.replace(/ [^ ]* [^ ]*$/, '...');
          });
          if (!wasCutted) {
            currentHeight -= angular.element('.news-close').height();
            wasCutted = true;
          }
          cut();
        } else {
          var readyText = angular.element("#builder").html();
          $scope.news.items[i].shortcontent = readyText;
          if (!angular.element(".row" + i).hasClass("news-open")) {
            $scope.news.items[i].content = $scope.news.items[i].shortcontent;
          } else {
            $scope.news.items[i].content = $scope.news.items[i].realcontent;
          }
        }
      };
      cut();

      if (wasCutted) {
        angular.element('.row' + i + ' .news-more').show();
      } else {
        angular.element('.row' + i + ' .news-more').hide();
      }
    }
  };
  $scope.checkWindowSize = function (force) {
    force = force || false;
    var displayWidth = $(window).width();
    if (!force && $scope.displayWidth && $scope.displayWidth === displayWidth)
      return;
    $scope.displayWidth = displayWidth;

    var header = angular.element(".news-content h2");
    var headerHeight = header.height() + parseInt(header.css("margin-top")) + parseInt(header.css("margin-bottom"));
    for (var i = 0; i < $scope.news.items.length; i++) {
      var currentHeight = Math.floor(angular.element(".news-row").width() * 0.5 * $scope.news.items[i]['ratio'] - headerHeight);
      if (!angular.element(".row" + i).hasClass("news-open")) {
        angular.element(".row" + i + " .news-content .content").height(currentHeight);
      }
    }
    if ($scope.resizeTimeout) {
      clearTimeout($scope.resizeTimeout);
      $scope.resizeTimeout = setTimeout(function () {
        $scope.cutContent();
        $scope.$apply();
      }, 200);
    }
    else {
      $scope.cutContent();
      $scope.resizeTimeout = setTimeout(function () {
      }, 0);
    }
  }
});

app.controller('courses', function ($scope, $location, $http) {
  angular.element(document).ready(function () {
    angular.element('.green-submenu a').click(function () {
      var hash = angular.element(this).attr('href').replace("#", "");
      $location.hash(hash);
    });
    angular.element('.linkcontaining a').click(function () {
      var hash = angular.element(this).attr('href').replace("#", "");
      $location.hash(hash);
    });
  });

  $scope.anketa = {
    name: '',
    age: '',
    phone: '',
    mail: '',
    comment: ''
  };

  $scope.validateForm = function (isValid) {
    if (isValid) {
      angular.element('form').remove();
      angular.element('.notice').show();

      $http({
        method: 'POST',
        url: 'backend/saveData.php',
        data: $.param({anketa: $scope.anketa}),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      });
    }
  }
});

app.controller('potential', function ($scope, $location, $window, $timeout) {
  angular.element(document).ready(function () {
    angular.element('.green-submenu a').click(function () {
      var hash = angular.element(this).attr('href').replace("#", "");
      $location.hash(hash);
    });

    angular.element($window).bind('resize', function () {
      var width = angular.element($window).width();
      var srcwidth = (width < 900) ? 900 : (width < 1024) ? 1024 : (width < 1366) ? 1366 : 1920;
      angular.element('.potential-slider').css({'background-image': 'url(img/poly' + srcwidth + '.png)'});
    });

    angular.element('.pot-more').click(function () {
      angular.element(this).parent().addClass('open-potential');
    });

    angular.element('.pot-close').click(function () {
      angular.element(this).parent().removeClass('open-potential');
    });

    var imgUrl = angular.element('.potential-bgr').css('background-image');
    var newsrc = imgUrl.substring(4, imgUrl.length - 1).replace(/\"/g, "");
    var img = new Image();

    img.onload = function () {
      angular.element('.potential-slider').css({'background-image': 'url(' + img.src + ')'});
    };
    img.src = newsrc;
    // redefining funciton translate() from potential controller
    $scope.redefineTranslationCallback(function () {
      if ($location.$$path.match(/\/potential/g)) {
        $timeout(function () {
          angular.element(".owl-item.cloned .first-slide-potential").html(angular.element(".owl-item:not(.cloned) .first-slide-potential").html());
          angular.element(".owl-item.cloned .last-slide-potential").html(angular.element(".owl-item:not(.cloned) .last-slide-potential").html())
        })
      }
    })
  })
});

app.controller('first', function ($scope, $location) {
  for (var datePart in $scope.birthday) {
    $scope.birthday.datePart = localStorage.getItem(datePart) !== null ? parseInt(localStorage.getItem(datePart)) : '';
  }

  for (var line in $scope.student) {
    if (localStorage.getItem(line) !== null) {
      $scope.student[line] = (line !== 'birthday') ?
          localStorage.getItem(line) :
          new Date(localStorage.getItem(line));
    }
  }

  $scope.submitForm = function (isValid) {
    if ($scope.birthday.day > new Date($scope.birthday.year, ~~$scope.birthday.month.id + 1, 0).getDate()) {
      $scope.dayerror = true;
      return false;
    }
    $scope.dayerror = false;

    if (isValid) {
      var birthday = new Date($scope.birthday.year, $scope.birthday.month.id, $scope.birthday.day);
      $scope.setIteminLS('birthday', birthday);
      var today = new Date();
      if (today.getFullYear() - birthday.getFullYear() <= 17) {
        $scope.student.course = $scope.translation.schoolgroup;
        $scope.setIteminLS('course', $scope.translation.schoolgroup);
        $location.path('/main-school');
      } else {
        $location.path('/main');
      }
    }
  }

});

app.controller('check', function ($scope, $location) {
  if ($scope.checkIfNameAgeEmpty()) {
    $location.path('/anketa');
    return;
  }
  $scope.choose = function (page) {
    if (page !== 'choose') {
      // $scope.student.course = $scope.translation.schoolgroup;
      $scope.student.course = 'Школьная группа';
    }
    $location.path(page);
  }
});

app.controller('course', function ($scope, $location) {
  if ($scope.checkIfNameAgeEmpty()) {
    $location.path('/anketa');
    return;
  }
  $scope.chooseCourse = function (c) {
    $scope.student.course = c;
    $location.path('/main');
  }
});

app.controller('main', function ($scope, $location, $http, FileUploader) {

  $scope.interests = [];
  $scope.interests.frontend = false;
  $scope.interests.backend = false;
  $scope.interests.mobile = false;
  $scope.interests.otherinput = '';
  $scope.emailerror = false;

  $scope.isEmailInDB = function (email) {
    if (email && email.length) {
      $http({
        method: 'POST',
        url: 'backend/isEmailExistInDB.php',
        data: $.param({email: email}),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).then(function successCallback(response) {
        $scope.emailerror = response['data'];
      });
      $scope.setIteminLS('email', email);
    }
  };

  for (var item in $scope.student) {
    if (localStorage.getItem(item) !== null) {
      if (item === 'birthday') {
        $scope.student[item] = new Date(localStorage.getItem(item));
      } else if (item === 'email') {
        $scope.student[item] = localStorage.getItem(item);
        $scope.isEmailInDB($scope.student[item]);
      }
      else {
        $scope.student[item] = localStorage.getItem(item);
      }
    }
    $scope.isEmailInDB($scope.student.email);
  }

  function isTrue(bool) {
    return bool == 'true' ? true : false;
  }

  for (var item in $scope.interests) {
    if (localStorage.getItem(item) !== null) {
      $scope.interests[item] = (item === 'otherinput') ?
          localStorage.getItem(item) :
          isTrue(localStorage.getItem(item));
    }
  }

  if ($scope.checkIfNameAgeEmpty()) {
    $location.path('/anketa');
    return;
  }

  $scope.interestRequire = function () {
    return (!$scope.interests.frontend && !$scope.interests.backend && !$scope.interests.mobile && $scope.interests.otherinput == '' );
  };

  /*function transliterateCharLC(char){
          return xchange[char] || char;
  };*/

  //transliterate world to a lower case string
  function transliterateWordLC(word, lang) {

    var firstLiteraXchange = {};
    var xchange = {};
    //exchange litera at the beginning of words
    var firstLiteraXchangeUA = {"є": "ye", "ї": "yi", "й": "y", "ю": "yu", "я": "ya"};
    //exchange litera which is not at the beginning of words
    var xchangeUA = {
      "а": "a",
      "б": "b",
      "в": "v",
      "г": "h",
      "ґ": "g",
      "д": "d",
      "е": "e",
      "є": "ie",
      "ж": "zh",
      "з": "z",
      "зг": "zgh",
      "и": "y",
      "і": "i",
      "ї": "i",
      "й": "i",
      "к": "k",
      "л": "l",
      "м": "m",
      "н": "n",
      "о": "o",
      "п": "p",
      "р": "r",
      "с": "s",
      "т": "t",
      "у": "u",
      "ф": "f",
      "х": "kh",
      "ц": "ts",
      "ч": "ch",
      "ш": "sh",
      "щ": "shch",
      "ю": "iu",
      "я": "ia"
    };

    //exchange litera at the beginning of words
    var firstLiteraXchangeRU = {};
    //exchange litera which is not at the beginning of words
    var xchangeRU = {
      "а": "a",
      "б": "b",
      "в": "v",
      "г": "g",
      "д": "d",
      "е": "e",
      "ё": "e",
      "ж": "zh",
      "з": "z",
      "и": "i",
      "й": "i",
      "к": "k",
      "л": "l",
      "м": "m",
      "н": "n",
      "о": "o",
      "п": "p",
      "р": "r",
      "с": "s",
      "т": "t",
      "у": "u",
      "ф": "f",
      "х": "kh",
      "ц": "ts",
      "ч": "ch",
      "ш": "sh",
      "щ": "shch",
      "ы": "y",
      "э": "e",
      "ю": "iu",
      "я": "ia"
    };

    var arrayOfChar = word.split('');

    switch (lang) {
      case 'ua':
        firstLiteraXchange = firstLiteraXchangeUA;
        xchange = xchangeUA;
        while (arrayOfChar.indexOf("з") > -1 && arrayOfChar[arrayOfChar.indexOf('з') + 1] == "г") {
          var point = arrayOfChar.indexOf('з');
          var nextpoint = point + 1;
          arrayOfChar[point] = "зг";
          arrayOfChar.splice(nextpoint, 1);
        }
        break;
      default:
        firstLiteraXchange = firstLiteraXchangeRU;
        xchange = xchangeRU;
    }

    return arrayOfChar.map(function (char, index) {
      if (index === 0) {
        return firstLiteraXchange[char] || xchange[char] || "";
      } else {
        return xchange[char] || "";
      }
    }).join('');
  }

  function generateNickname(name, surname, lang) {
    surname = surname.toLowerCase();
    name = name.toLowerCase();
    var nickname = '';

    switch (lang) {
      case 'ru':
      case 'ua':
        if (/[а-яА-ЯёЁіІїЇєЄґҐ]/i.test(name)) {
          if (/[ёЁ]/i.test(name)) {
            nickname = transliterateWordLC(name, 'ru').charAt(0) + transliterateWordLC(surname, 'ru');
          } else if (/[іІїЇєЄґҐ]/i.test(name)) {
            nickname = transliterateWordLC(name, 'ua').charAt(0) + transliterateWordLC(surname, 'ua');
          } else {
            nickname = transliterateWordLC(name, $scope.selectedLanguage).charAt(0) + transliterateWordLC(surname, $scope.selectedLanguage);
          }
        } else {
          nickname = name.charAt(0) + surname;
        }

        break;

      case 'en':
        if (/[a-zA-Z]/i.test(name)) {
          nickname = name.charAt(0) + surname;
        } else {
          if (/[ёЁ]/i.test(name)) {
            nickname = transliterateWordLC(name, 'ru').charAt(0) + transliterateWordLC(surname, 'ru');
          } else if (/[іІїЇєЄґҐ]/i.test(name)) {
            nickname = transliterateWordLC(name, 'ua').charAt(0) + transliterateWordLC(surname, 'ua');
          } else {
            nickname = transliterateWordLC(name, $scope.selectedLanguage).charAt(0) + transliterateWordLC(surname, $scope.selectedLanguage);
          }
        }
        break;

      default:
        nickname = name.charAt(0) + surname;
    }
    return nickname;
  }

  var uploader = $scope.uploader = new FileUploader({
    url: 'backend/upload.php',
    formData: {email: $scope.student.email}
  });

  uploader.filters.push({
    name: 'imageFilter',
    fn: function (item /*{File|FileLikeObject}*/, options) {
      var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
      return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
    }
  });

  uploader.onSuccessItem = function (fileItem, response, status, headers) {
    angular.element('.addImg').empty();
    angular.element('.thnx').show();
    angular.element('.load-but').empty();
  };

  var controller = $scope.controller = {
    isImage: function (item) {
      var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
      return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
    }
  };

  $scope.submitForm = function (isValid) {

    if (uploader.queue[0] === undefined) {
      angular.element('html, body').animate({
        scrollTop: angular.element(".has-error").first().offset().top - 100
      }, 300);
      return;
    }

    if (isValid) {
      // subscribing for mailchimp emails
      var list = [];
      var listArray = document.querySelectorAll('.list:checked');
      for (var item in listArray) {
        listArray[item].value !== undefined ? list.push(listArray[item].value) : '';
      }
      //if no list checked don't send request
      if (list[0] !== undefined) {
        $http({
          method: 'POST',
          url: 'backend/mailchimper.php',
          data: $.param({email: $scope.student.email, list: list}),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
      }

      for (var interest in $scope.interests) {
        if ($scope.interests[interest] != false && $scope.interests[interest] != '') {
          if ($scope.interests[interest]) {
            $scope.student.course += $scope.student.course != '' ? ", " + interest : interest;
          } else {
            $scope.student.course += $scope.student.course != '' ? ", " + $scope.interests[interest] : $scope.interests[interest];
          }
        }
      }

      $scope.student.nickname = generateNickname($scope.student.name, $scope.student.surname, $scope.selectedLanguage);

      //php like date
      $scope.student.birthday = $scope.student.birthday.getFullYear() +
          '-' + ($scope.student.birthday.getMonth() + 1) +
          '-' + $scope.student.birthday.getDate();

      $http({
        method: 'POST',
        url: 'backend/saveData.php',
        data: $.param({student: $scope.student}),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).then(function successCallback(response) {
        //add photo to this student
        uploader.queue[0].formData.email = $scope.student.email;
        uploader.queue[0].upload();
      }, function errorCallback(response) {

      });

      localStorage.clear();
      $location.path('/last');
    } else {
      angular.element('html, body').animate({
        scrollTop: angular.element(".has-error").first().offset().top - 100
      }, 300);
    }
  }
});

app.controller('last', function ($scope, $http, $location) {
  if ($scope.checkIfNameAgeEmpty()) {
    $location.path('/main');
  }
});

app.controller('camera', function ($location, $scope, $timeout) {
  $timeout(function () {
    if ($scope.configurations && $scope.configurations.camera === "off") {
      $location.path('/');
    }
  })
});

app.factory('News', function ($http) {
  var News = function () {
    this.items = [];
    this.busy = false;
    this.after = 0;
    this.count = null;
    this.lang = 'ru';
  };

  News.prototype.nextPage = function (lang, callback) {
    this.lang = lang;
    if (this.busy) return;
    this.busy = true;
    if (this.after < this.count || this.count === null) {
      $http({
        method: 'POST',
        url: 'backend/getNews.php',
        data: $.param({'limit': this.after}),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (msg) {
        this.after += 10;
        this.busy = false;
        if (typeof(msg) === 'object') {
          for (var i = 0; i < msg.news.length; i++) {
            this.items.push(msg.news[i]);
          }
          for (var j = 0; j < this.items.length; j++) {
            this.items[j].realcontent = this.items[j]['content_' + this.lang];
            this.items[j].title = this.items[j]['title_' + this.lang];
          }
        }
        this.count = msg.counter;
        // it work's with 1ms, but not work with $timeout, weird but true
        setTimeout(function () {
          callback();
        }, 1);
      }.bind(this));
    }
  };

  return News;
});
