var startClient = function(io, params) {
	var socket = io.connect("http://shpp.me:7777");
	socket.on("connect", 	function(msg) {
		params.onConnect();
	});
	socket.on("disconnect", function(msg) { 
		params.onDisconnect();
	});
	socket.on("message", 	function(msg) {
		params.onMessage(msg);
	});

	return {
		sendMessage: function(message) {socket.emit("message", message);},
		sendAngleLeft: function(left)  {socket.emit("left", left);},
		sendAngleTop:  function(top)   {socket.emit("top", top);}
	};
}