function setPixel(imageData, x, y, color, a) {
    var r = color.r;
    var g = color.g;
    var b = color.b;
    
    index = (x + y * imageData.width) * 4;
    imageData.data[index + 0] = r;
    imageData.data[index + 1] = g;
    imageData.data[index + 2] = b;
    imageData.data[index + 3] = a;
}
function changePixel(imageData, x, y, color, a, aC) {
    var r = color.r;
    var g = color.g;
    var b = color.b;

    index = (x + y * imageData.width) * 4;
    
    var a2 = imageData.data[index + 3] / 255;
    var r2 = imageData.data[index + 0];
    var g2 = imageData.data[index + 1];
    var b2 = imageData.data[index + 2];
    
    a = a/255;
    var alpha = a + a2 * (1 - a); 

    var red   = (r*a + r2*a2*(1-a))/alpha;
    var green = (g*a + g2*a2*(1-a))/alpha;
    var blue  = (b*a + b2*a2*(1-a))/alpha;
    
    imageData.data[index + 0] = ~~red;   
    imageData.data[index + 1] = ~~green; 
    imageData.data[index + 2] = ~~blue;  
    imageData.data[index + 3] = ~~(alpha*255);

}
function convert(degrees) {
  	return degrees * Math.PI / 180;
}
function limit(value, a, b) {
	return (value <= b && value >= a);
}
function getMax(array) {
	var a = array[0];
	for (var i = 0; i < array.length; ++i)
		if (array[i] > a)
			a = array[i];

	return a;
}
function getMin(array) {
	var a = array[0];
	for (var i = 0; i < array.length; ++i)
		if (array[i] < a)
			a = array[i];

	return a;
}

function startJoystick(params) {

	function getParam(nameParameter, defaultParam) {
		if (nameParameter in params)
			return params[nameParameter];
		return defaultParam;
	}

	var colorBorder =     getParam("colorBorder", 	  {r: 100, g: 100, b: 100});
	var colorCursor = 	  getParam("colorCursor", 	  {r: 150, g: 150, b: 150});
	var colorCursorLine = getParam("colorCursorLine", {r: 200, g: 200, b: 200});

	var borderAlpha =     getParam("alphaBorder", 255);
	var cursorLineAlpha = getParam("alphaLineCursor", 255);
	var cursorAlpha =     getParam("alphaCursor", 255);

	var cursorXj = getParam("startX", 50);
	var cursorYj = getParam("startY", 50);

	var elementName = getParam("canvas", "joystick");
	element = document.getElementById('joystick');
	var c = element.getContext("2d");

	var width  = element.width;
	var height = element.height;

	imageData = c.createImageData(width, height);

	var joyWidth =  getParam("joyWidth", 100);
	var joyHeight = getParam("joyHeight", 50);
	var dugaFrom = 	0;
	var dugaTo = 	180;
	var iskrivl =   getParam("iskrl", 10);

	var paddingX =  getParam("padX", 3);
	var paddingY =  getParam("padY", 3);

	function draw(x, y) {
		y = 100 - y;
		imageData = c.createImageData(width, height);

		var cameraYPer100 = y; 
		var cameraXPer100 = x;
		

		var cameraYPercent = (cameraYPer100-50)/50*1;
		var cursorWidth = 20;
		var cursorX = (cameraXPer100/100 * ((joyWidth-cursorWidth-5)))+5;

		var cameraRectHeight = (getParam("cameraHeight", 1/2))/1.0001;
		var cameraYBorder = cameraRectHeight/2;
		var cameraPointMaxPercent = 1 - cameraYBorder * 2;
		var pointPercent = cameraPointMaxPercent * cameraYPercent;
		pointPercent = pointPercent * 1.1;
		
		for (var i = 0; i < joyWidth; i++) {

			var gradus = dugaFrom + (i/joyWidth * (dugaTo-dugaFrom));
			var rad = convert(gradus);

			var s = Math.sin(rad)/1.001; 
			
			var yv = (s * iskrivl) + paddingY;
			var yn = (-s * iskrivl) + joyHeight + paddingY;
			var x  = i + paddingX;

			var centerY   = (yn + yv)/2;
			var currentH  = (yn - yv);
			var pointYPos = centerY - currentH/2*pointPercent;
			var smeshenie = cameraYBorder*currentH;
			
			var p1 = (pointYPos - smeshenie);
			var p2 = (pointYPos + smeshenie);
			
			var alpha = borderAlpha;
			var k1b = Math.ceil(yv) - yv; 
			var k2b = yn - Math.floor(yn);

			var k11 = k1b;
			var k21 = k2b;

			if (k1b == 0 && k2b == 0) {
				k1b = 1;
				k2b = 0;
			}

			if (i < 3 || i > joyWidth-4) {
				if (yv < (paddingY))
					yv = (paddingY);

				if (yn > (joyHeight + paddingY))
					yn = (joyHeight + paddingY);

				for (var j = yv-1; j < yn; j++) 
					setPixel(imageData, x, ~~j, colorBorder, borderAlpha);
		    }

			if (i > 2 && i < joyWidth-3) {
			
				var alpha = cursorLineAlpha;
				var color = colorCursorLine;
				var isInCursor = (x >= cursorX && x <= cursorX + cursorWidth);

				if (isInCursor) {
					for (var j = yv; j < p1+1; j++) 
						setPixel(imageData, x, ~~j, color, alpha);
					for (var j = p1+1; j < p2-1; j++) 
						setPixel(imageData, x, ~~j, colorCursor, cursorAlpha);
					for (var j = p2-1; j < yn; j++) 
						setPixel(imageData, x, ~~j, color, alpha);
				} else {
					for (var j = p1; j < p2-1; j++) 
						setPixel(imageData, x, ~~j, color, alpha);
				}

				var k1 = Math.ceil(p1) - p1;
				var k2 = p2 - Math.floor(p2);

				if (k1 == 0 && k2 == 0) {
					k1 = 1;
					k2 = 0;
				}

				if (isInCursor) {
					changePixel(imageData, x, ~~(p1)  , color, ~~(alpha*k1));
					changePixel(imageData, x, ~~(p2)-1, color, ~~(alpha*k2));
				} else {
					setPixel(imageData, x, ~~(p1)  , color, ~~(alpha*k1));
					setPixel(imageData, x, ~~(p2)-1, color, ~~(alpha*k2));
				}

			}

			changePixel(imageData, x, (~~yv)-3, colorBorder, borderAlpha * k1b);
			changePixel(imageData, x, (~~yv)-2, colorBorder, borderAlpha);
			changePixel(imageData, x, (~~yv)-1, colorBorder, borderAlpha);
			changePixel(imageData, x, (~~yv),   colorBorder, ~~(borderAlpha * (1-k11)));
			
			changePixel(imageData, x, (~~yn)-1, colorBorder, ~~(borderAlpha * (1-k21)));
			changePixel(imageData, x, (~~yn), 	colorBorder, borderAlpha);
			changePixel(imageData, x, (~~yn)+1, colorBorder, borderAlpha);
			changePixel(imageData, x, (~~yn)+2, colorBorder, borderAlpha * k2b);

			
		}
		
		c.putImageData(imageData, 0, 0);
	
	}

	var curX = 0;
	var curY = 0;
	var xToValue = 0;
	var yToValue = 0;
	var isPressed = false;
	var jElement = $("#" + elementName);
	var jElementDiv = $("#" + getParam("canvas_div", (elementName + "_div")));
	jElement.mousedown(function(e) {
		if (e.which === 1 && !isPressed) {
			curX = e.pageX - jElement.offset().left;
			curY = e.pageY - jElement.offset().top;

			isPressed = true;
		}
	});
	$("body").mousemove(function(e) {
		if (e.which === 1 && isPressed) {
			var x = e.pageX - jElement.offset().left;
			var y = e.pageY - jElement.offset().top;

			xToValue = cursorXj - ((curX - x) / (joyWidth)  * 100);
			yToValue = cursorYj - ((curY - y) / (joyHeight) * 100);
			
			if (xToValue > 100)
			 	xToValue = 100;
			else if (xToValue < 0)
				xToValue = 0;

			if (yToValue > 100)
			 	yToValue = 100;
			else if (yToValue < 0)
				yToValue = 0;

			if (limit(xToValue, 0, 100) && limit(yToValue, 0, 100)) {
				draw(xToValue, yToValue);
				params.onCommand(xToValue, 100 - yToValue);
			}

			$("body").addClass("dragging");
		}	

	});
	$("body").mouseup(function(e) {
		if (e.which === 1 && isPressed) {
			isPressed = false;	
			cursorXj = xToValue;
			cursorYj = yToValue;
		}

		$("body").removeClass("dragging");
	});

	jElementDiv.mouseout(function() {
		if (!isPressed)
			params.onLeave();
	}).mouseover(function() {
  		params.onEnter();	
	});

	draw(cursorXj, cursorYj);

	return {draw: draw};
}
