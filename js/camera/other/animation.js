function setToText(tag, text, colorClass) {
	tag.html(text);
	if (colorClass == "red")
		tag.removeClass("green")
			.addClass("red");
	else if (colorClass == "green")
		tag.removeClass("red")
			.addClass("green");
} 
function animateProgressBar(tag, barTag, duration) {
	tag.stop();
	barTag.show();
	tag.css({width: "0px"});
	tag.animate({width: '150px'}, duration, 'linear', function() {
		barTag.hide();
	});
}
function hide(tags) {
	for (var i = 0; i < tags.length; ++i)
		tags[i].hide();
}
function show(tags) {
	for (var i = 0; i < tags.length; ++i)
		tags[i].show();
}