function startWebrtc(elementName, onStream, onEnd) {
    var connection = new RTCMultiConnection("shpp_camera");
    connection.session = {
        audio: true,
        video: true,
        oneway: true
    };
    connection.onstream = function(e) {
        e.mediaElement.width =  640;
        e.mediaElement.height = 480;
        e.mediaElement.style.zIndex = 2;
        e.mediaElement.controls = false;
        var videoContainer = document.getElementById(elementName);
        videoContainer.insertBefore(e.mediaElement, videoContainer.firstChild);

        window.setTimeout(onStream, 2000);
        onStream();
    };                                            
    connection.onstreamended = function(e) {
        e.mediaElement.style.opacity = 0;
        setTimeout(function() {
            if (e.mediaElement.parentNode) 
                e.mediaElement.parentNode.removeChild(e.mediaElement);
        }, 1000);
        onEnd();
    };
    var sessions = {};
    connection.onNewSession = function(session) {

        if (sessions[session.sessionid]) return;
        sessions[session.sessionid] = session;

        if (session.sessionid == "camera")
            session.join();
    };
    
    connection.connect();
    function closeConnection() {
        connection.disconnect();
        document.getElementById(elementName).innerHTML = "";
    }

    return {
        disconnect: closeConnection
    };
}