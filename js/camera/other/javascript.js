
$.getScript('js/animation.js', function() {
    $.getScript('js/joystick.js', function() {


        hide([ $(".status_bar"), $(".btn") ]);  

        $(document).ready(function() {

            $(".video-content").css({visibility: "visible"});

            var ua = navigator.userAgent.toLowerCase();
            var isAndroid = ua.indexOf("android") > -1; 
            var isIOS = /(ipad|iphone|ipod)/g.test(ua); 
            var m3u8 = "http://shpp.me:1935/live-ll/r/playlist.m3u8";
            var poster = "img/overlay.jpg";
                
            if (isAndroid || isIOS) {
                var e = document.getElementById("video");
                e.innerHTML = '<video width="600" height="400" src="'+m3u8+'" controls="controls"></video>'; 
                e.style.display = 'block';
                document.getElementById("btnControl").style.display = "none";

                hide([ $("#status_con") ]);
            } else {
                startVideoPlayer();

                joystick = startJoystick({
                    "canvas":           "joystick", 
                    "canvas_div":       "playerbox", 
                    "colorBorder":      {r: 220, g: 220, b: 220},
                    "colorCursor":      {r: 180, g: 180, b: 180},
                    "colorCursorLine":  {r: 150, g: 150, b: 150},
                    "alphaCursor":      220,
                    "alphaBorder":      255,
                    "alphaLineCursor":  50,
                    "startX":           50,
                    "startY":           50,
                    "padX":             4,
                    "padY":             3,
                    "iskrl":            10,
                    "joyWidth":         100,
                    "joyHeight":        50,
                    "cameraHeight":     1/2, 
                    onCommand: function(x, y) {
                        x = 100 - x;
                        sendToServerLeft(~~(x));
                        sendToServerTop(~~(y));
                    },
                    onLeave: function() {
                        $("#joystick").stop();
                        $("#joystick").css({opacity: 1});
                        $("#joystick").animate({opacity: 0, duration: 5});
                    },
                    onEnter: function() {
                        $("#joystick").stop()
                        $("#joystick").animate({opacity: 1, duration: 5});
                    }
                });


                $("#btnControl").click(function () {
                    setToText( $("#status_con"), "Getting control...", "green");
                    sendToServer("I WANT");
                });

                hide([ $("#joystick"), $("#status_con") ]);

                var client = startClient(io, {

                    onConnect: function() {
                        console.log("Connected to server.");
                        
                        hide([ $(".status_bar"), $("#status_con") ]);
                        show([ $("#btnControl")]);
                        $("#status_con").html("");

                        if (joystick != null)
                            joystick.draw(50, 50);
                    },

                    onDisconnect: function() {
                        console.log("DISCONNECT");
                        setToText( $("#status_con"), "Error. Server disconnect.", "red");

                        hide([ $(".status_bar"), $(".btn"), $("#joystick") ]);
                        show([ $("#status_con") ]);
                        stopVideoWebrtc();
                    },

                    onMessage: function(msg) {
                        console.log("Server: " + msg);
                        if (msg == "YOU CAN") {

                            setToText( $("#status_con"), "Control! You have 1 minute.", "green");
                            animateProgressBar( $("#bar"), $(".status_bar"), 60000);

                            show([ $("#joystick") ]);
                            hide([ $("#btnControl") ]); 

                            startVideoWebrtc();

                        } else if (msg == "YOU CAN YET 1m") {

                            setToText( $("#status_con"), "Control! You have 1 minute.", "green");
                            animateProgressBar( $("#bar"), $(".status_bar"), 60000);

                            show([ $("#joystick") ]);
                            hide([ $("#btnControl") ]);

                        } else if (msg == "YOU CAN NO LIMIT") {

                            setToText( $("#status_con"), "Control!", "green");

                            show([ $("#joystick") ]);   
                            hide([ $(".status_bar"), $("#btnControl") ]);

                            startVideoWebrtc();

                        } else if (msg == "END") {

                            setToText( $("#status_con"), "End of control.", "red");

                            hide([ $(".status_bar"), $("#joystick") ]); 
                            show([ $("#btnControl") ]);

                            stopVideoWebrtc();

                        } else if (msg.substring(0, 8) == ("MAX WAIT")) {

                            var min = msg.substring(8, msg.length);
                            
                            animateProgressBar( $("#bar"), $(".status_bar"), parseInt(min) * 60000);
                            setToText( $("#status_con"), "Wait maximum " + min + " minutes.", "red");

                            hide([ $(".btn"), $("#joystick") ]);

                        }
                    }
                });

                function sendToServer(msg) {
                    console.log("I:      " + msg);
                    client.sendMessage(msg);
                }

                function sendToServerLeft(angle) {client.sendAngleLeft(angle);} 
                function sendToServerTop(angle)  {client.sendAngleTop(angle);} 
                
                var webrtc = null; 
                var joystick = null;
                
                function startVideoWebrtc() {
                    if (checkWebrtcSupport()) {
                        webrtc = startWebrtc("video", function () {
                            $("#video").css({display: "block"});
                            $("#video-flash").css({display: "none"});
                        }, function () {stopVideoWebrtc()});

                    }
                }
                function stopVideoWebrtc() {
                    $("#video").css({display: "none"});
                    $("#video-flash").css({display: "block"});
                    webrtc.disconnect();
                    webrtc = null;
                }

                function checkWebrtcSupport() {
                    return DetectRTC.isWebRTCSupported;
                }

                

            }
        });
    });
});

function startVideoPlayer() {
    jwplayer("player_wrapper").setup({
        file: "rtmp://shpp.me/live-ll/_definst_/r",
        height: 480,
        width:  640,
        buffer: 0, 
        autostart: true,
        stretching: "fit",
        rtmp: {
          bufferlength: 0.1 
        }
    });
}
