<?php
function random_string($length)
{
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    return substr(str_shuffle($chars), 0, $length);
}

if (!empty($_FILES)) {
    $tempPath = $_FILES['file']['tmp_name'];
    $randomstr = random_string(6);
    $filename = md5(time()) . $randomstr;
    $uploadPath = 'students/' . $filename . '.png';
    $email = implode('', array_values($_POST));
    move_uploaded_file($tempPath, $uploadPath);

    include 'dbAnketa.php';
    $p = mysqli_real_escape_string($con, $uploadPath);
    $s = mysqli_real_escape_string($con, $email);
    mysqli_query($con, "UPDATE `users` SET photo = '$p' WHERE email = '$email'");
    $answer = array('answer' => 'File transfer completed');
    $json = json_encode($answer);

    echo $json;
}

$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "http://portal.programming.kr.ua/api/create-photo",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "secret=keepMeWithYouToSurvive&email=$email",
    CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded",
    ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);
