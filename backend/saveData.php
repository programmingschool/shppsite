<?php
error_reporting(0); // quick fix
if (isset($_POST['student'])) {
    include __DIR__ . '/dbAnketa.php';
} else {
    include __DIR__ . '/db.php';
}

if (isset($_POST['anketa'])) {
    foreach ($_POST['anketa'] as $key => $value) {
        $data[$key] = htmlspecialchars(strip_tags($value), ENT_QUOTES);
    }
    $keys = array_keys($data);
    array_walk($keys, function (&$string) use ($con) {
        $string = mysqli_real_escape_string($con, $string);
    });
    $columns = implode(', ', $keys);

    array_walk($data, function (&$string) use ($con) {
        $string = mysqli_real_escape_string($con, $string);
    });
    $values = implode("', '", array_values($data));

    mysqli_query($con, "INSERT INTO `form` ($columns) VALUES ('$values')");
}

if (isset($_POST['mailtext'])) {
    $mail = mysqli_real_escape_string($con, htmlspecialchars(strip_tags($_POST['mailtext']), ENT_QUOTES));

    mysqli_query($con, "INSERT INTO `questions` (question) VALUES ('$mail')");
}
if (isset($_POST['student'])) {
    foreach ($_POST['student'] as $key => $value) {
        $data[$key] = htmlspecialchars(strip_tags($value), ENT_QUOTES);
    }

    $data['nickname'] = unique_nickname($data['nickname'], $con);

    //Sets the default timezone
    date_default_timezone_set('Europe/Kiev');
    //get datetime for form
    $data['created_at'] = date('Y-m-d H:i:s');
    $birthday = date('Y-m-d', strtotime($data['birthday']));

    $keys = array_keys($data);
    array_walk($keys, function (&$string) use ($con) {
        $string = mysqli_real_escape_string($con, $string);
    });
    $columns = implode(', ', $keys);

    array_walk($data, function (&$string) use ($con) {
        $string = mysqli_real_escape_string($con, $string);
    });
    $values = implode("', '", array_values($data));

    file_put_contents(
        '../xprivate/backups/form_backup_' . date('Y-m-d') . '.log',
        json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) . "\r\n",
        FILE_APPEND
    );

    $save = mysqli_query($con, "INSERT INTO `users` ($columns) VALUES ('$values')");
    //send responsive_email
    $result_pack = [];
    $result_pack['status'] = false;
    $result_pack['msg'] = 'Ответное письмо не было отправлено';

    $to = $data['email'];

    $subject = 'Cпасибо за заполненную анкету | programming.kr.ua';

    $from = new DateTime($birthday);
    $age = $from->diff(new DateTime('today'))->y;
    //create message
    if ($data['studyform'] === 'online') {
        include 'views/responsive_email_online.php';
        $message = ob_get_clean();
    } else {
        $responsiveEmailPath = 'views/responsive_email_view.php';
	if (in_array($age, [8,9])) {
	    $subject = 'Дякуємо за заповнення анкети | programming.kr.ua';
	    $message = file_get_contents('views/responsive_email_child_8_9.php');
	} elseif ($age < 12) {
            //include 'views/responsive_email_child.php';
            $message = file_get_contents('views/responsive_email_new_child_ua.php');
        } elseif ($age >= 12 && $age <= 16) {
            include 'views/responsive_email_schoolgroup.php';
            $message = ob_get_clean();
        } else {
            include $responsiveEmailPath;
            $message = ob_get_clean();
        }
    }

    // content-type
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type:text/html;charset=UTF-8' . "\r\n";
    //from
    $headers .= 'From: Школа программирования Ш++ <anketa@programming.kr.ua>' . "\r\n";

    $result = mail($to, $subject, $message, $headers);
    $subject = 'Новая заявка';

    $to = new DateTime('today');
    $to = 'anketa@programming.kr.ua';
    $message = '';
    $message .= '<b>Новая заявка:&nbsp;</b>' . $data['name'] . ' ' . $data['surname'] . '<br>';
    $message .= '<b>Возраст:&nbsp;</b>' . $age . '<br>';
    $message .= '<b>Email:&nbsp;</b>' . $data['email'] . '<br>';
    $message .= '<b>Телефон:&nbsp;</b>' . $data['phone'] . '<br>';
    if ($data['parent_name']) {
        $message .= '<b>Родитель:&nbsp;</b>' . $data['parent_name'] . ' ( ' . $data['parent_phone'] . ' )<br />';
    }
    $message .= '<b>Группа:&nbsp;</b>' . $data['course'] . '<br>';
    $message .= '<b>English:&nbsp;</b>' . $data['english'] . '<br>';
    if ($data['city']) {
        $message .= '<b>Город:&nbsp;</b>' . $data['city'] . '<br>';
    }
    if ($data['studyform']) {
        $message .= '<b>Форма обучения:&nbsp;</b>' . $data['studyform'] . '<br>';
    }
    if ($data['lessontime']) {
        $message .= '<b>Время обучения:&nbsp;</b>' . $data['lessontime'] . '<br>';
    }
    if ($data['hours']) {
        $message .= '<b>Готов учиться часов в неделю:&nbsp;</b>' . $data['hours'] . '<br>';
    }
    if ($data['occupation']) {
        $message .= '<b>Род занятий:&nbsp;</b>' . $data['occupation'] . '<br>';
    }
    if ($data['portfolio']) {
        $message .= '<b>Опыт:&nbsp;</b>' . $data['portfolio'] . '<br>';
    }
    $message .= '<b>О себе:&nbsp;</b>' . nl2br($data['about']) . '<br>';
    $message .= '<b>Дата заполнения:&nbsp;</b>' . $data['created_at'] . '<br>';
    mail($to, $subject, $message, $headers);

    if ($result) {
        $result_pack['status'] = true;
        $result_pack['msg'] = 'The response mail sent successfully';
    } else {
        $result_pack['status'] = false;
        $result_pack['msg'] = 'The response mail sent unsuccessfully';
    }
    
    // notification to slack
    try {
        $user_id = null;
        $query = "SELECT id FROM users WHERE email=?";
        if ($stmt = mysqli_prepare($con, $query)) {
            mysqli_stmt_bind_param($stmt, "s", $data['email']);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt, $user_id);
            mysqli_stmt_fetch($stmt);
            mysqli_stmt_close($stmt);
        }
	
	if ($age < 12) {
	    $message = "`дети`"; 
	} elseif ($age >= 12 && $age <= 16) {
	    $message = "`школьники`";
	} else {
	    $message = "`взрослые`";
	}

	$age_sufix = ending($age, "год", "года", "лет");

        $message .= " " . $data['name'] . " - " . $age . " " . $age_sufix. " - id ". $user_id;
        if (strcmp($data['city'], 'Кировоград') == 0 || strcmp($data['city'], 'Кропивницкий') == 0) {
            $message .= " - г." . $data['city'];
        } else {
            $message .= " - г.*" . $data['city'] . "*";
        }
        if ($data['occupation']) {
            $message .= " - " . $data['occupation'];
        }
        
        $message .= "\n_О себе:_ " . urlencode($data['about']);

        if ($data['portfolio']) {
            $message .= "\n_Работы:_ " . $data['portfolio'];
        }
        
        $data_string = "payload=" . json_encode(['text' => $message,
                'channel' => "#anketa-feed", 'username' => 'анкетобот', 'icon_emoji' => ":ghost:"]);
        
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://hooks.slack.com/services/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");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        $answer = curl_exec($ch);
        curl_close($ch);
        
        $slackLogPath = __DIR__ . '/logs';
        file_put_contents($slackLogPath . '/slack_logs.txt', date('Y-m-d H:i:s').' '.$answer.PHP_EOL, FILE_APPEND);
        
    } catch (Exception $e) {
	file_put_contents($slackLogPath . '/slack_error.txt', date('Y-m-d H:i:s').' '.$e.PHP_EOL, FILE_APPEND);
    }


    $errorBagName = 'errors';
    list($err, $res) = afterSave($data['email']);
    if ($err) {
        $result_pack[$errorBagName]['after_save'] = $err;
    }

    $logPath = __DIR__ . '/logs/after_save';
    if (is_writable($logPath)) {
        $logFilename = $logPath . '/' . date('Y-m-d') . '_after_save_response.log';
        if (false === @file_put_contents(
                $logFilename,
                date('Y-m-d H:i:s') . ' INFO: email: ' . urldecode($data['email']) . PHP_EOL .
                'res: ' . var_export($res, true) . PHP_EOL .
                'err: ' . var_export($result_pack[$errorBagName], true) . PHP_EOL,
                FILE_APPEND
            )) {
            $result_pack[$errorBagName]['log_save'] = "Can't save log at path";
        }
    } else {
        $result_pack[$errorBagName]['log_permissions'] = 'Log path is not writable';
    }

    echo json_encode($result_pack);
}

/**
 * @param string $email
 * @return array
 */
function afterSave($email)
{
    include __DIR__ . '/after_save.config.php';
    if (!isset($afterSaveConfig, $afterSaveConfig['PRIVATE'], $afterSaveConfig['URL'])) {
        return ['No config for after save provided', ''];
    }
    $curl = curl_init();
    curl_setopt_array($curl, [
        CURLOPT_URL => $afterSaveConfig['URL'] . '?' . http_build_query([
                'email' => $email,
                'private_key' => $afterSaveConfig['PRIVATE']
            ]),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => 'UTF-8',
        CURLOPT_MAXREDIRS => 5,
        CURLOPT_TIMEOUT => 10,
        CURLOPT_CUSTOMREQUEST => 'GET',
    ]);
    $res = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    return [$err, $res];
}

function unique_nickname($nickname, $con)
{
    $isUnique = false;
    $try = 0;
    $unique_nickname = $nickname;
    while (!$isUnique && $try < 1000) {
        $query = 'SELECT id FROM users WHERE nickname = "' . $unique_nickname . '"';
        $result = mysqli_query($con, $query);
        if (!mysqli_num_rows($result)) {
            $isUnique = true;
        } else if (!$isUnique && $try === 0) {
            $unique_nickname = $nickname . date('Y');
            $try++;
        } else {
            $unique_nickname = $nickname . ++$try;
        }
    }
    return $unique_nickname;
}

function ending($num, $first, $second, $third) {
    $num = (int)$num;
   
    if ($num < 21 && $num > 4)
        return $third;
 
    $num = $num%10;
 
    if ($num == 1)
        return $first;
    if ($num > 1 && $num < 5)
        return $second;
 
    return $third;
}

