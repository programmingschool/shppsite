<?php
  if (isset($_POST['archiveTheseIDs'])) {
    include 'db.php';
    $res = array();

    $archiveIDs = json_decode($_POST['archiveTheseIDs'],true);
    $archiveIDsNum = count($archiveIDs);
    if($archiveIDsNum > 0) {
      $archiveIDsStr = "";
      foreach ($archiveIDs as $key => $id) {
        $archiveIDsStr .= $id;
        if($key < ($archiveIDsNum - 1)) {
          $archiveIDsStr .= ", ";
        }
      }
      mysqli_query($con, "UPDATE `students` SET `students`.`archived` = 1 WHERE `students`.`id` IN ($archiveIDsStr)");
    }
    else {
      $res = ['error' => "Found no ID"];
    }

    mysqli_close($con);

    echo json_encode($res);
  }

  
