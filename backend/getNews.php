<?php
include 'db.php';

if ($con->connect_error) {
    echo json_encode(["err" => $con->connect_error, "response" => "", "code" => 503]);
    return;
}

if (isset($_POST['limit'])) {
    $limit = intval($_POST['limit']);
    $highLimit = $limit + 10;
    $limitQuery = " LIMIT " . $limit . ", " . $highLimit;
} else {
    $limitQuery = " LIMIT 0, 10";
}
$sql_query = "SELECT * FROM `news` ORDER BY `news`.`data` DESC" . $limitQuery;
$result = mysqli_query($con, $sql_query);
$obj = new \stdClass;
$news = [];
while ($row = mysqli_fetch_assoc($result)) {
    if (preg_match("/^video\:/", $row['photo'])) {
        $row['video'] = preg_replace("/^video\:/", "", $row['photo']);
        $row['photo'] = "";
        array_push($news, $row);
    } else {
        $row['photo'] = explode(", ", $row['photo']);
        array_push($news, $row);
    }
}
$obj->news = $news;

//getting count
$sql_query = "SELECT COUNT(*) as counter FROM `news`";
$result = mysqli_query($con, $sql_query);

while ($row = mysqli_fetch_assoc($result)) {
    $obj->counter = $row['counter'];
}

echo json_encode($obj);
return;
