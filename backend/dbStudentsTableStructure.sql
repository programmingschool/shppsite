--  MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `students`;
CREATE TABLE `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `birthday` date NOT NULL,
  `studyform` varchar(50) NOT NULL,
  `hours` varchar(25) NOT NULL,
  `lessontime` varchar(50) NOT NULL,
  `about` text NOT NULL,
  `nickname` varchar(150) NOT NULL,
  `course` varchar(200) NOT NULL,
  `city` text NOT NULL,
  `occupation` varchar(80) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `parent_name` varchar(100) NOT NULL,
  `parent_phone` varchar(50) NOT NULL,
  `email` varchar(200) NOT NULL,
  `skype` varchar(200) NOT NULL,
  `english` varchar(200) NOT NULL,
  `photo` text NOT NULL,
  `portfolio` text NOT NULL,
  `sendmail` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2016-02-17 14:56:13
