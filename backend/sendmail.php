<?php
$mail = htmlspecialchars(strip_tags($_POST['mailtext']), ENT_QUOTES);
$tomail = file_get_contents('tomail.json');
$tomail = json_decode($tomail,true);
array_push($tomail['message'], $mail);
$tomail = json_encode($tomail, JSON_PRETTY_PRINT);
file_put_contents('tomail.json', $tomail);