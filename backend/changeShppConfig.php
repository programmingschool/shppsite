<?php
if (isset($_POST['changeCameraConfig']) && in_array($_POST['changeCameraConfig'], array("on", "off"))) {
    $config = file_get_contents('../configs.json');
    $config = json_decode($config, true);
    if (is_array($config)) {
        $newCameraVal = $_POST['changeCameraConfig'];
        if ($config['camera'] != $newCameraVal) {
            $config['camera'] = $newCameraVal;
            $config = json_encode($config);
            file_put_contents('../configs.json', $config);
            echo 'CHANGE programming.kr.ua: config camera value was changed to ' . $newCameraVal;
        } else {
            echo 'SUCCESS programming.kr.ua: config camera value is already ' . $newCameraVal;
        }
    } else {
        echo 'ERROR programming.kr.ua: wrong type of configs.json file: ' . gettype($config);
    }
}
