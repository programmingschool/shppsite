<?php
  include 'db.php';

  $data = array();
  
  if (isset($_POST['form'])) {
    $result = mysqli_query($con, "SELECT * FROM form");
    
    while($row = mysqli_fetch_assoc($result)) {
      array_push($data, $row);
    } 
  }
  if (isset($_POST['questions'])) {
    $result = mysqli_query($con, "SELECT * FROM questions");
    
    while($row = mysqli_fetch_assoc($result)) {
      array_push($data, $row);
    } 
  }
  if (isset($_POST['students'])) {
    $result = mysqli_query($con, "SELECT * FROM `students` WHERE `students`.`archived` = 0 ORDER BY `students`.`id` DESC");
    
    while($row = mysqli_fetch_assoc($result)) {
      array_push($data, $row);
    }
  }
  if (isset($_POST['students_all'])) {
    $result = mysqli_query($con, "SELECT * FROM `students` ORDER BY `students`.`id` DESC");
    
    while($row = mysqli_fetch_assoc($result)) {
      array_push($data, $row);
    }
  }
  if (isset($_POST['news'])) {
    $result = mysqli_query($con, "SELECT * FROM `news` ORDER BY `news`.`data` DESC");
    while ($row = mysqli_fetch_assoc($result)) {
      if (preg_match("/^video\:/", $row['photo'])) {
        $row['video'] = preg_replace("/^video\:/", "", $row['photo']);
        $row['photo'] = "";
        array_push($data, $row);
      } else {
        $row['photo'] = explode(", ", $row['photo']);
        array_push($data, $row);
      }
    }
  }

  $data = json_encode($data);
  echo $data;
