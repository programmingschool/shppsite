<?php
$anketa = file_get_contents('students.json');
$anketa = json_decode($anketa);
foreach ($_POST['student'] as $key => $value) {
    $data[$key] = htmlspecialchars(strip_tags($value), ENT_QUOTES);
}
$data['skypeCheck'] = 'Да';
$data['gmailCheck'] = 'Да';
array_push($anketa, $data);
$anketa = json_encode($anketa, JSON_PRETTY_PRINT);
file_put_contents('students.json', $anketa);

$name = "<b><i>Имя:&nbsp;</i></b>".$data['name']."<br />";
$age = "<b><i>Возраст:&nbsp;</i></b>".$data['age']."<br />";
$course = "<b><i>Направление:&nbsp;</i></b>".$data['course']."<br />";
$address = "<b><i>Адрес:&nbsp;</i></b>".$data['address']."<br />";
$phone = "<b><i>Телефон:&nbsp;</i></b>".$data['phone']."<br />";
$education = "<b><i>Образование:&nbsp;</i></b>".$data['education']."<br />";
$gmail = "<b><i>Почта:&nbsp;</i></b>".$data['gmail']."<br />";
$skype = ($data['skype'] == '') ? '' : "<b><i>skype:&nbsp;</i></b>".$data['skype']."<br />";
$parphone = ($data['parphone'] == '') ? '' : "<b><i>Телефон родителя:&nbsp;</i></b>".$data['parphone']."<br />";
$english = ($data['english'] == '') ? '' : "<b><i>English:&nbsp;</i></b>".$data['english']."<br />";
$skills = ($data['skills'] == '') ? '' : "<b><i>Навыки:&nbsp;</i></b>".$data['skills']."<br />";
$dest = ($data['dest'] == '') ? '' : "<b><i>Настрой:&nbsp;</i></b>".$data['dest']."<br />";

$formail = $name.$age.$phone.$course.$address.$education.$gmail.$skype.$parphone.$english.$skills.$dest;

$tomail = file_get_contents('tomail.json');
$tomail = json_decode($tomail,true);
array_push($tomail['mainform'], $formail);
$tomail = json_encode($tomail, JSON_PRETTY_PRINT);
file_put_contents('tomail.json', $tomail);



