<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cпасибо за заполненную анкету | programming.kr.ua</title>
</head>
<body style="color: black">
  <div style="margin-bottom: 10px;">
    Здравствуйте, <?= $data['name']; ?>.
  </div>
  <div style="margin-bottom: 10px;">
    Спасибо за заполненную анкету!
  </div>
  <div style="margin-bottom: 10px;">
    Мы свяжемся с вами в течение 2-4 недель, чтобы договориться о нашем первом занятии.<br>
    Если что - звоните :) <a href="tel:+380502011180">(050 20 111 80)</a>
  </div>
  <div style="margin-bottom: 10px;">
    <i>Ваша <a style="color: black; text-decoration: underline" href="http://programming.kr.ua">Ш++</a></i>
  </div>
</body>
</html>
