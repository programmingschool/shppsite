<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cпасибо за заполненную анкету | programming.kr.ua</title>
</head>
<body style="color: black">
  <div style="margin-bottom: 10px;">
    Здравствуйте, <?= $data['name']; ?>.
  </div>
  <div style="margin-bottom: 10px;">
    Спасибо за заполненную анкету!
  </div>
  <div style="margin-bottom: 10px;">
    Набор в следующую группу будет проводиться в течение 3-5 месяцев. Мы свяжемся с вами, чтобы пригласить на обучение.<br>
    Если что - звоните :) <a href="tel:+380502011180">(050 20 111 80)</a>
  </div>
  <div style="margin-bottom: 10px;">
    <i>Ваша <a style="color: black; text-decoration: underline" href="http://programming.kr.ua">Ш++</a></i>
  </div>
</body>
</html>
