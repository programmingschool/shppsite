<?php
include 'db.php';

$forms = [];
$mainforms = [];
$messages = [];

$mailmessages = '';
$mailforms = '';
$mailmainforms = '';
$mailwarning = '<p><b>ВНИМАНИЕ!</b> ОТВЕЧАЯ НА ЭТО ПИСЬМО, ТЫ ОТВЕЧАЕШЬ НАШЕЙ КОМАНДЕ, А НЕ
ЧЕЛОВЕКУ КОТОРЫЙ ЗАПОЛНИЛ АНКЕТУ!</p>';

$resultQ = mysqli_query($con, "SELECT * FROM questions WHERE sendmail = '0'");
$resultF = mysqli_query($con, "SELECT * FROM form WHERE sendmail = '0'");
$resultM = mysqli_query($con, "SELECT * FROM students WHERE sendmail = '0'");

while ($row = mysqli_fetch_assoc($resultQ)) {
    $messages[] = $row;
}
while ($row = mysqli_fetch_assoc($resultF)) {
    $forms[] = $row;
}
while ($row = mysqli_fetch_assoc($resultM)) {
    $mainforms[] = $row;
}

$messagesCounter = count($messages);
$formsCounter = count($forms);
$mainformsCounter = count($mainforms);

if ($formsCounter > 0 || $mainformsCounter > 0 || $messagesCounter > 0) {
    if ($formsCounter > 0) {
        $mailforms = '<h3>Заполнена анкета</h3>';

        for ($i = 0; $i < $formsCounter; $i++) {
            $mailforms .= '<p>';
            foreach ($forms[$i] as $key => $value) {
                if ($key !== 'id' && $key !== 'sendmail' && $value != '') {
                    $mailforms .= template($key, $value);
                }
            }
            $mailforms .= '</p>';
        }
    }
    if ($mainformsCounter > 0) {
        $mailmainforms = '<h3>Новый Ученик</h3>';

        for ($i = 0; $i < $mainformsCounter; $i++) {
            $mailmainforms .= '<p>';
            foreach ($mainforms[$i] as $key => $value) {
                if ($key !== 'id' && $key !== 'sendmail' && $key !== 'skypeCheck' && $key !== 'gmailCheck' && $key !== 'photo' && $value != '') {
                    $mailmainforms .= template($key, $value);
                }
            }
            $mailmainforms .= '</p>';
        }
    }
    if ($messagesCounter > 0) {
        $mailmessages = '<h3>Задан вопрос</h3>';

        for ($i = 0; $i < $messagesCounter; $i++) {
            $mailmessages = $mailmessages . '<p><i>' . $messages[$i]['question'] . '</i></p>';
        }
    }

    require_once 'lib/swift_required.php';

    $transport = Swift_MailTransport::newInstance();
    $message = Swift_Message::newInstance();
    $message->setTo([
        // "oliinyk.artem@gmail.com" => "Artem",
        // "smirnovanastya1602@gmail.com" => "Nastya",
        'info@programming.kr.ua' => 'Ш++'
    ]);
    $message->setContentType('text/html');
    $message->setSubject('Был задан вопрос');
    $message->setBody($mailforms . $mailmainforms . $mailmessages . $mailwarning);
    $message->setFrom('info@programming.kr.ua', "Ш++");
    $mailer = Swift_Mailer::newInstance($transport);
    $mailer->send($message);

    mysqli_query($con, "UPDATE questions SET sendmail = '1' WHERE sendmail = '0'");
    mysqli_query($con, "UPDATE form SET sendmail = '1' WHERE sendmail = '0'");
    mysqli_query($con, "UPDATE students SET sendmail = '1' WHERE sendmail = '0'");
}

function template($title, $body)
{
    return '<b><i>' . $title . ':&nbsp;</i></b>' . $body . '<br>';
}
