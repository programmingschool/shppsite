-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Час створення: Лют 26 2016 р., 16:00
-- Версія сервера: 5.5.47-0ubuntu0.14.04.1
-- Версія PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База даних: `cool_portal`
--

-- --------------------------------------------------------

--
-- Структура таблиці `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `direction_id` int(10) unsigned NOT NULL,
  `isEnabled` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `groups_direction_id_foreign` (`direction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `group_directions`
--

CREATE TABLE IF NOT EXISTS `group_directions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `direction` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=4 ;

--
-- Дамп даних таблиці `group_directions`
--

INSERT INTO `group_directions` (`id`, `direction`, `created_at`, `updated_at`) VALUES
(1, 'csa', NULL, NULL),
(2, 'csb', NULL, NULL),
(3, 'prom', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `group_history`
--

CREATE TABLE IF NOT EXISTS `group_history` (
  `student_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `group_history_student_id_index` (`student_id`),
  KEY `group_history_group_id_index` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `mentorship`
--

CREATE TABLE IF NOT EXISTS `mentorship` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mentor_id` int(10) unsigned NOT NULL,
  `student_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mentorship_mentor_id_index` (`mentor_id`),
  KEY `mentorship_student_id_index` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Дамп даних таблиці `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_100000_create_password_resets_table', 1),
('2016_01_21_105726_CreateRolesTable', 1),
('2016_01_21_121233_create_state_table', 1),
('2016_01_21_138000_create_users_table', 1),
('2016_01_22_095607_create_posts_category_table', 1),
('2016_01_22_105946_create_mentorship_table', 1),
('2016_01_22_133034_create_posts_table', 1),
('2016_02_15_150128_create_users_last_visit_column', 1),
('2016_02_15_154852_create_group_direction_table', 1),
('2016_02_15_160155_create_groups_table', 1),
('2016_02_16_122641_create_group_history_table', 1),
('2016_02_17_123409_create_post_comments_table', 1);

-- --------------------------------------------------------

--
-- Структура таблиці `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(10) unsigned NOT NULL,
  `post_author_id` int(10) unsigned NOT NULL,
  `postText` text COLLATE utf8_general_ci NOT NULL,
  `visibility_for_owner` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `posts_owner_id_foreign` (`owner_id`),
  KEY `posts_post_author_id_foreign` (`post_author_id`),
  KEY `posts_category_id_foreign` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `posts_categories`
--

CREATE TABLE IF NOT EXISTS `posts_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=4 ;

--
-- Дамп даних таблиці `posts_categories`
--

INSERT INTO `posts_categories` (`id`, `category_name`) VALUES
(1, 'author_post'),
(2, 'mentor_post'),
(3, 'autopost');

-- --------------------------------------------------------

--
-- Структура таблиці `post_comments`
--

CREATE TABLE IF NOT EXISTS `post_comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `comment_author_id` int(10) unsigned NOT NULL,
  `commentText` text COLLATE utf8_general_ci NOT NULL,
  `visibility_for_owner` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `post_comments_post_id_foreign` (`post_id`),
  KEY `post_comments_comment_author_id_foreign` (`comment_author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=10 ;

--
-- Дамп даних таблиці `roles`
--

INSERT INTO `roles` (`id`, `role_name`) VALUES
(1, 'signed_up'),
(2, 'waiting_test_result'),
(3, 'waiting_interview'),
(4, 'waiting_interview_results'),
(5, 'failed'),
(6, 'student'),
(7, 'listener'),
(8, 'kid'),
(9, 'unexpected');

-- --------------------------------------------------------

--
-- Структура таблиці `states`
--

CREATE TABLE IF NOT EXISTS `states` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `state_name` varchar(255) COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=4 ;

--
-- Дамп даних таблиці `states`
--

INSERT INTO `states` (`id`, `state_name`) VALUES
(1, 'enabled'),
(2, 'read_only'),
(3, 'disabled');

-- --------------------------------------------------------

--
-- Структура таблиці `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nickname` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `skype` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `parent_phone` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `parent_name` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `photo` text COLLATE utf8_general_ci NOT NULL,
  `birthday` date NOT NULL,
  `vk` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `github` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `linkedin` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `course` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `english` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `about` text COLLATE utf8_general_ci NOT NULL,
  `studyform` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `lessontime` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `occupation` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `hours` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `portfolio` text COLLATE utf8_general_ci NOT NULL,
  `programming_experience` text COLLATE utf8_general_ci NOT NULL,
  `mentor` tinyint(1) NOT NULL DEFAULT '0',
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `graduated` tinyint(1) NOT NULL DEFAULT '0',
  `isMale` tinyint(1) NOT NULL DEFAULT '1',
  `role_id` int(10) unsigned NOT NULL DEFAULT '1',
  `state_id` int(10) unsigned NOT NULL DEFAULT '3',
  `password` varchar(60) COLLATE utf8_general_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_general_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  KEY `users_state_id_foreign` (`state_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=5 ;

--
-- Дамп даних таблиці `users`
--

INSERT INTO `users` (`id`, `nickname`, `email`, `name`, `surname`, `skype`, `phone`, `parent_phone`, `parent_name`, `photo`, `birthday`, `vk`, `facebook`, `instagram`, `twitter`, `github`, `city`, `linkedin`, `course`, `english`, `about`, `studyform`, `lessontime`, `occupation`, `hours`, `portfolio`, `programming_experience`, `mentor`, `admin`, `graduated`, `isMale`, `role_id`, `state_id`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '', 'admin@admin.com', 'Admin', 'Admin', '', '', '', '', '', '2013-05-12', '', '', '', '', '', '', '', 'education', '', 'I am School', '', '', '', '16', '', '', 1, 1, 1, 1, 9, 1, '$2y$10$vQOZA0a4pfYR7xJ18LdsAexGwNA/7Z21ZQKkD5mW4TLxbUvlOXOXO', NULL, NULL, NULL);

--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `groups_direction_id_foreign` FOREIGN KEY (`direction_id`) REFERENCES `group_directions` (`id`);

--
-- Обмеження зовнішнього ключа таблиці `group_history`
--
ALTER TABLE `group_history`
  ADD CONSTRAINT `group_history_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `group_history_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `mentorship`
--
ALTER TABLE `mentorship`
  ADD CONSTRAINT `mentorship_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `mentorship_mentor_id_foreign` FOREIGN KEY (`mentor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `posts_categories` (`id`),
  ADD CONSTRAINT `posts_owner_id_foreign` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `posts_post_author_id_foreign` FOREIGN KEY (`post_author_id`) REFERENCES `users` (`id`);

--
-- Обмеження зовнішнього ключа таблиці `post_comments`
--
ALTER TABLE `post_comments`
  ADD CONSTRAINT `post_comments_comment_author_id_foreign` FOREIGN KEY (`comment_author_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `post_comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`);

--
-- Обмеження зовнішнього ключа таблиці `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`),
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
