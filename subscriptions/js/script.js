$('#submitter').click(function(event) {
	sendRequest();
});

$('#email').keypress(function(event) {
	if(event.keyCode == 13){
		sendRequest();
    }
});

function sendRequest(){
	email = $('#email').val();
	list = $('input:checkbox:checked.list').map(function () {
		return this.value;
	}).get();

	if(!email){
		Materialize.toast('Введите свой электронный адрес в форму', 4000);
		$('#email').focus();	
		return;
	}else{	
		if(!validateEmail(email)){
			Materialize.toast('Введите коректный электронный адрес в форму', 4000);
			$('#email').focus();	
			return;
		}
	}

	if(list.length === 0){
		Materialize.toast('Сначала выберите подписку', 4000);
		$('.sublist').effect( "shake" );
		return;
	}
 	
 	$('.loadtaker').show();
	$.ajax({
		url: 'mailchimper.php',
		type: 'POST',
		data: {
			email,
			list
		},
	})
	.done(function(echolot) {
		Materialize.toast('Спасибо за подписку!', 4000);
		$('.form').hide('slow');
		$('.thanks').show('slow');
		console.log("success" + '\n' + echolot);
	})
	.fail(function() {
 		$('.loadtaker').hide();
		console.log("error");
	})
	.always(function() {
 		$('.loadtaker').hide();
		console.log("complete");
	});

}

function validateEmail(email) {
	//regex from habrahabr
    var re = /^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(?:\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@(?:[a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$/;
    return re.test(email);
}