<?php

$list = $_POST['list'];

foreach ($list as $key => $listId) {
        syncMailchimp($listId);
}

function syncMailchimp($listId) {
        $apiKey = '423c7c5952e8574e9f0594d3bed8a08a';

        $url = 'https://api.mailerlite.com/api/v2/groups/'.$listId.'/subscribers';

        $json = json_encode(['email' => $_POST['email']]);

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', "X-MailerLite-ApiKey: $apiKey"]);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

        $result   = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        echo $httpCode;
}
