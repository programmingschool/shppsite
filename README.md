## programming.kr.ua site

Built on Angular 1

-------------

### Requirements
-   Apache

-------------

### Setup
Clone the repository.

Set up the virtual host(on windows):    
- create folder `C:\vhosts`
- create a subfolder inside `C:\vhosts` for each virtual host that you want to add to your Apache server.(e.g. `C:\vhosts\local.programming.kr.ua`)
- open `C:\WINDOWS\system32\drivers\etc\hosts` in any text editor as administrator. At the end of the file enter the following:
```
127.0.0.1   local.programming.kr.ua
```
- save the `hosts` file, and close it
- open the main Apache configuration file, `httpd.conf`, in a text editor. It's in the Apache conf folder. If you're using XAMPP, the file is located at `C:\xampp\apache\conf\httpd.conf`. Scroll down to the Supplemental configuration section at the end, and uncomment the section around line 500 to look like this:
```
#Virtual hosts
Include conf/extra/httpd-vhosts.conf
```
- save `httpd.conf` and close it
- open  `extra\httpd-vhosts.conf` in Notepad or a text editor. If you're using XAMPP, the location is `C:\xampp\apache\conf\extra\httpd-vhosts.conf`. You must remove the hash mark from the beginning of the line that contains the following directive:
```
NameVirtualHost *.80
```
- add this to the blank space at line 15:
```
<Directory C:/vhosts>
  Require all granted
</Directory>
```
- add folowing to the end of the file
```
<VirtualHost *:80>
    DocumentRoot "C:/xampp/htdocs/shppsite"
    ServerName local.programming.kr.ua
</VirtualHost>
```
- save `httpd-vhosts.conf`, and restart Apache. 

Open `http://local.programming.kr.ua` to make sure installation was successful.

------------
To setup the backend you have to add database configuration to the files `db_default.php` and `dbAnketa_default.php` and rename them to `db.php` and `dbAnketa.php`

For after save callback copy-rename `after_save.config.php.example` to `after_save.config.php` and set your hook url and private key 
